enum Genre {
  drama = "DRAMA",
  comedy = "COMEDY",
  documental = "DOCUMENAL"
}

abstract class Show {

  constructor(
    public readonly name: string,
    public readonly genre: Genre,
    public readonly releaseDate: Date,
    public readonly durationMin: number
  ) {
    this.name = name;
    this.genre = genre;
    this.releaseDate = releaseDate;
    this.durationMin = durationMin;
  }

  abstract getDuration(): number
}


class Movie extends Show {

  constructor(name: string,
              genre: Genre,
              releaseDate: Date,
              duration: number
  ) {
    super(name,
      genre,
      releaseDate, duration);
  }

  getDuration() {
    return this.durationMin
  }
}

class Episode extends Show {

  constructor(name: string,
              genre: Genre,
              releaseDate: Date, duration: number) {
    super(name,
      genre,
      releaseDate, duration);
  }

  override getDuration() {
    return this.durationMin
  }
}

class Series extends Show {
  private readonly episodes: Episode[]

  constructor(episodes: Episode[], name: string,
              genre: Genre,
              releaseDate: Date, duration: number) {
    super(name,
      genre,
      releaseDate, duration);
    this.episodes = episodes
  }

  get getEpisodes() {
    return this.episodes;
  }

  override getDuration(): number {
    return this.episodes.map(e => e.durationMin).reduce((acc, val) => acc + val, 0);
  }
}

class StreamingService {
  public readonly shows: Show[] = []
  public readonly viewsByShowNames: Map<string, number> = new Map<string, number>()

  constructor(public readonly name: string) {
    this.name = name
  }

  public addShows(...shows:Show[]){
    
  this.shows.push(...shows)
  }

  public addShow(show: Show) {
    const isShowExist = this.shows.some(s => s.name === show.name)
    if(!isShowExist)
    this.shows.push(show)
    else throw new Error(`Show ${show.name} still exists`)
  }

  public getMostViewedShowsOfYear(year: number): Show[] {

    return [...this.viewsByShowNames.entries()].filter(([name]) =>
      this.shows.find(s => s.name === name)?.releaseDate.getFullYear() === year
    ).sort(([, countView1], [,countView2]) => {
      return countView2 - countView1
    })
      .flatMap(([showName])=> this.shows.filter(s => s.name === showName)).slice(0, 9);

  }

  public getMostViewedShowsOfGenre(genre: Genre) {
    return [...this.viewsByShowNames.entries()].filter(([name]) =>
      this.shows.find(s => s.name === name)?.genre === genre
    ).sort(([, countView1], [,countView2]) => {
      return countView2 - countView1
    })
      .flatMap(([showName])=> this.shows.filter(s => s.name === showName)).slice(0, 9);
  }
}

class Subscription {

  constructor(public readonly streamingService: StreamingService) {
    this.streamingService = streamingService
  }

  public watch(showName: string) {
    const show = this.streamingService.shows.find((s) => s.name === showName);

    if(!show) {
      throw new Error(`Show ${showName} not found`)
    }

    console.log(`Show ${showName} is watch`)


    let preViewsShowCount = this.streamingService.viewsByShowNames.get(show.name);

    this.streamingService
      .viewsByShowNames
      .set(show.name, preViewsShowCount ? ++preViewsShowCount : 1)
  }

  public getRecommendationTrending() {
    const now = new Date()
    const mostViewedShowsOfYear = this.streamingService.getMostViewedShowsOfYear(now.getFullYear());
    return mostViewedShowsOfYear[Math.floor(Math.random() * mostViewedShowsOfYear.length)];
  }

  public getRecommendationByGenre(genre: Genre) {
    const mostViewedShowsOfGenre = this.streamingService.getMostViewedShowsOfGenre(genre);
    return mostViewedShowsOfGenre[Math.floor(Math.random() * mostViewedShowsOfGenre.length)];
  }
}

class User {
  private readonly subscriptions: Subscription[] = []

  constructor(public readonly name: string) {
    this.name = name
  }

  public subscribe(streamingService: StreamingService): Subscription {
    const subscription = new Subscription(streamingService);

    const streamingServiceByName = this.subscriptions.find(s => s.streamingService.name === streamingService.name);

    if(streamingServiceByName) {
      throw new Error("streamingService already subscribe")
    }

    this.subscriptions.push(subscription)
    return subscription
  }

  get getSubscriptions() {
    return this.subscriptions
  }
}

//Створення нового фільму

const move1= new Movie('Зоряні війни', Genre.documental, new Date(), 120)
const move2= new Movie('Полночь', Genre.drama, new Date(), 100)
const move3= new Movie('Чарлі чаплін', Genre.comedy, new Date(), 80)
console.log(move1, 'move1')
console.log(move2, 'move2')
console.log(move3, 'move3')
//Створення нового Епізоду

const episode1 = new Episode('1', Genre.documental, new Date(1977,6,25), 30 )
const episode2= new Episode('2', Genre.documental, new Date(1999,1,25), 40 )
const episode3= new Episode('3', Genre.documental, new Date(), 50 )
const episode4= new Episode('4', Genre.documental, new Date(), 55 )
const episode5= new Episode('5', Genre.documental, new Date(), 54 )

console.log(episode1, 'episode1')
console.log(episode2, 'episode2')
console.log(episode3, 'episode3')
console.log(episode4, 'episode4')
console.log(episode5, 'episode5')

const series1 = new Series([episode1,episode2,episode3,episode4],'TypeScript',Genre.documental, new Date(),120)
const series2 = new Series([episode1,episode2,episode3],'JS',Genre.documental, new Date(),200)
const series3 = new Series([episode1,episode2,episode3],'JS-Html',Genre.documental, new Date(2021, 10, 5),200)

//Створення нового стрім. сервісу
const streamS1 = new StreamingService('Megago')
const streamS2 = new StreamingService('netflix')
console.log(streamS1,'stream1')
console.log(streamS2,'stream2')

streamS1.addShows(move1,move2)
streamS2.addShows(move2,move3)

//у списку шоу на стрімінгу не має бути дублікатів

try{
  streamS1.addShow(move1)
}
catch(error){
  console.log("Error", error)
}
console.log(streamS1.shows, '207')


//створення нового User

const user1= new User('Nick')
console.log(user1)
const user2= new User('Andrew')
console.log(user2)
const user3= new User('Masha')
console.log(user3)

//підписує користувача на стрімінговий сервіс
const subscrib1 = user1.subscribe(streamS1)
const subscrib2 = user1.subscribe(streamS2)
const subscrib3 = user3.subscribe(streamS1)
//користувач не може повторно оформити підписку на один і той же сервіс
try{
  const subscrib2 = user1.subscribe(streamS1)
}catch(error){
  console.log('Error, You have already been subscribed ', error)
}

//перегляд шоу 
subscrib1.watch('Зоряні війни')
subscrib1.watch('Зоряні війни')
subscrib1.watch('Зоряні війни')
subscrib1.watch('Полночь')
subscrib3.watch('Полночь')




//користувач не може дивитися шоу, якого не шснує на стрімі
try{
  subscrib1.watch('три товариша')
}
catch(error){
 console.log("Error, film doesnt exist", error)
}

console.log(subscrib1, 'subscrib1')


const mostviewedFilms = streamS1.getMostViewedShowsOfYear(2022)
console.log(mostviewedFilms,'mostviewedFilms')



const showsArr=streamS1.getMostViewedShowsOfYear(2022)
console.log(showsArr, 'showsArr')
console.log(streamS1.shows,'streamS1.shows')
const showArrGenre= streamS1.getMostViewedShowsOfGenre(Genre.documental)
console.log(showArrGenre)





