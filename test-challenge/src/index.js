"use strict";
var Genre;
(function (Genre) {
    Genre["drama"] = "DRAMA";
    Genre["comedy"] = "COMEDY";
    Genre["documental"] = "DOCUMENAL";
})(Genre || (Genre = {}));
class Show {
    name;
    genre;
    releaseDate;
    durationMin;
    constructor(name, genre, releaseDate, durationMin) {
        this.name = name;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.durationMin = durationMin;
        this.name = name;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.durationMin = durationMin;
    }
}
class Movie extends Show {
    constructor(name, genre, releaseDate, duration) {
        super(name, genre, releaseDate, duration);
    }
    getDuration() {
        return this.durationMin;
    }
}
class Episode extends Show {
    constructor(name, genre, releaseDate, duration) {
        super(name, genre, releaseDate, duration);
    }
    getDuration() {
        return this.durationMin;
    }
}
class Series extends Show {
    episodes;
    constructor(episodes, name, genre, releaseDate, duration) {
        super(name, genre, releaseDate, duration);
        this.episodes = episodes;
    }
    get getEpisodes() {
        return this.episodes;
    }
    getDuration() {
        return this.episodes.map(e => e.durationMin).reduce((acc, val) => acc + val, 0);
    }
}
class StreamingService {
    name;
    shows = [];
    viewsByShowNames = new Map();
    constructor(name) {
        this.name = name;
        this.name = name;
    }
    addShows(...shows) {
        this.shows.push(...shows);
    }
    addShow(show) {
        this.shows.push(show);
    }
    getMostViewedShowsOfYear(year) {
        return [...this.viewsByShowNames.entries()].filter(([name]) => this.shows.find(s => s.name === name)?.releaseDate.getFullYear() === year).sort(([, countView1], [, countView2]) => {
            return countView2 - countView1;
        })
            .flatMap(([showName]) => this.shows.filter(s => s.name === showName)).slice(0, 9);
    }
    getMostViewedShowsOfGenre(genre) {
        return [...this.viewsByShowNames.entries()].filter(([name]) => this.shows.find(s => s.name === name)?.genre === genre).sort(([, countView1], [, countView2]) => {
            return countView2 - countView1;
        })
            .flatMap(([showName]) => this.shows.filter(s => s.name === showName)).slice(0, 9);
    }
}
class Subscription {
    streamingService;
    constructor(streamingService) {
        this.streamingService = streamingService;
        this.streamingService = streamingService;
    }
    watch(showName) {
        const show = this.streamingService.shows.find((s) => s.name === showName);
        if (!show) {
            throw new Error(`Show ${showName} not found`);
        }
        console.log(`Show ${showName} is watch`);
        let preViewsShowCount = this.streamingService.viewsByShowNames.get(show.name);
        this.streamingService
            .viewsByShowNames
            .set(show.name, preViewsShowCount ? ++preViewsShowCount : 1);
    }
    getRecommendationTrending() {
        const now = new Date();
        const mostViewedShowsOfYear = this.streamingService.getMostViewedShowsOfYear(now.getFullYear());
        return mostViewedShowsOfYear[Math.floor(Math.random() * mostViewedShowsOfYear.length)];
    }
    getRecommendationByGenre(genre) {
        const mostViewedShowsOfGenre = this.streamingService.getMostViewedShowsOfGenre(genre);
        return mostViewedShowsOfGenre[Math.floor(Math.random() * mostViewedShowsOfGenre.length)];
    }
}
class User {
    name;
    subscriptions = [];
    constructor(name) {
        this.name = name;
        this.name = name;
    }
    subscribe(streamingService) {
        const subscription = new Subscription(streamingService);
        const streamingServiceByName = this.subscriptions.find(s => s.streamingService.name === streamingService.name);
        if (streamingServiceByName) {
            throw new Error("streamingService already subscribe");
        }
        this.subscriptions.push(subscription);
        return subscription;
    }
    get getSubscriptions() {
        return this.subscriptions;
    }
}
/* name: string,
              genre: Genre,
              releaseDate: Date,
              duration: number */
const move1 = new Movie('Звездные войны', Genre.documental, new Date(), 120);
const move2 = new Movie('Полночь', Genre.drama, new Date(), 100);
const move3 = new Movie('Чарли чаплин', Genre.comedy, new Date(), 80);
console.log(move1, 'move1');
console.log(move2, 'move2');
console.log(move3, 'move3');
const episode1 = new Episode('1', Genre.documental, new Date(), 30);
const episode2 = new Episode('2', Genre.documental, new Date(), 40);
const episode3 = new Episode('3', Genre.documental, new Date(), 50);
const episode4 = new Episode('4', Genre.documental, new Date(), 55);
const episode5 = new Episode('5', Genre.documental, new Date(), 54);
console.log(episode1, 'episode1');
console.log(episode2, 'episode2');
console.log(episode3, 'episode3');
console.log(episode4, 'episode4');
console.log(episode5, 'episode5');
const series1 = new Series([episode1, episode2, episode3, episode4], 'TypeScript', Genre.documental, new Date(), 120);
const series2 = new Series([episode1, episode2, episode3], 'JS', Genre.documental, new Date(), 200);
const series3 = new Series([episode1, episode2, episode3], 'JS-Html', Genre.documental, new Date(2021, 10, 5), 200);
const streamS1 = new StreamingService('Megago');
const streamS2 = new StreamingService('netflix');
console.log(streamS1, 'stream1');
console.log(streamS2, 'stream2');
streamS1.addShows(move1, move2);
streamS2.addShows(move2, move3);
console.log(streamS1.shows);
const user1 = new User('Nick');
console.log(user1);
const user2 = new User('Andrew');
console.log(user2);
const user3 = new User('Masha');
console.log(user3);
const subscrib1 = user1.subscribe(streamS1);
subscrib1.watch('Звездные войны');
subscrib1.watch('Звездные войны');
subscrib1.watch('Полночь');
console.log(subscrib1, 'subscrib1');
//console.log(JSON.stringify(streamS1))
const showsArr = streamS1.getMostViewedShowsOfYear(2022);
console.log(showsArr);
//console.log(user1)
