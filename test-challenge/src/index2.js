const Genre = {
  drama: "DRAMA",
  comedy: "COMEDY",
  documental: "DOCUMENAL"
}


class Show {
  constructor(
    name,
    genre,
    releaseDate
  ) {
    this.name;
    this.genre;
    this.releaseDate
  }

  getDuration() {

  }
}

class Movie extends Show {

  constructor(name,
              genre,
              releaseDate) {
    super(name,
      genre,
      releaseDate);
  }

  getDuration() {
    super.getDuration();
  }
}

class Episode extends Show {

  constructor(name,
              genre,
              releaseDate) {
    super(name,
      genre,
      releaseDate);
  }

  getDuration() {
    super.getDuration();
  }
}

class Series extends Show{

  constructor(episodes, name,
              genre,
              releaseDate) {
    super(name,
      genre,
      releaseDate);
    this.episodes = episodes
  }
}

class StreamingService {
  shows = []


  constructor(
    name
  ) {
    this.name = name
    this.viewsByShowNames = new Map()
  }

  getMostViewedShowsOfGenre() {

  }

  addShow(show) {
    this.shows.push(show)
  }

  getMostViewedShowsOfYear(year, number) {

  }
}

class Subscription {

  constructor(
    streamingService
  ) {
    this.streamingService = streamingService
  }

  watch(showName) {

  }

  getRecommendationTrending() {

  }

  getRecommendationByGenre() {
    return this.streamingService.getMostViewedShowsOfGenre()
  }
}

class User {
  constructor(name) {
    this.name = name
    this.subscriptions = []
  }

  subscribe(streamingService) {
    this.subscriptions.push(streamingService)
  }
}
