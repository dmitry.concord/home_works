import React, {useEffect} from 'react';
import { useDispatch,useSelector } from 'react-redux';
import FavoriteCard from '../../components/favoriteCards/favoriteCard';
import '../Home/home.css';
import CartIcon from '../../components/cartIcon/CartIcon';
import { updateLocalStorage } from '../../redux/actions/posts';

const Favorites = () => {
  const dispatch= useDispatch()
  useEffect(()=>{
    dispatch(updateLocalStorage())
  },[])

  const favorites = useSelector(({favorite})=> favorite.favorites)
  const productsInCart = useSelector(({productsInCart})=>productsInCart.productsInCart)
  
  return (<><CartIcon cartLenght={productsInCart.length}

  />
    <div className='items-wrapper'>
      FaVORITES
      <div className="items">   {favorites.map((item)=>(<div key={item.article}>
<FavoriteCard item={item} cardInFavorite={true} />

</div> ))}  
     </div>
    </div>
    </>);
};

export default Favorites;