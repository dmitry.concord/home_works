import React, { useEffect } from 'react';
import { useDispatch,useSelector } from 'react-redux';
import ProductList from '../../components/productList/ProductList';
import { getRequest } from '../../redux/actions/posts';
import CartIcon from '../../components/cartIcon/CartIcon';
import { updateLocalStorage } from '../../redux/actions/posts';


const Home = () => {
const dispatch=useDispatch()
  
  useEffect(()=>{
  dispatch(getRequest())
  dispatch(updateLocalStorage())
  },[])

  const productsInCart = useSelector(({productsInCart})=>productsInCart.productsInCart)
  //console.log(productsInCart)
  return (<><CartIcon cartLenght={productsInCart.length}

  />
  <div className='items-wrapper'>
    <ProductList/>
  </div></>
  );
};

export default Home;