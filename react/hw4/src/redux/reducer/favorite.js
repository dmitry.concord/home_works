
import {ADD_PRODUCTS_IN_FAVORITES,REMOVE_PRODUCTS_FROM_FAVORITES,UPDATE_FROM_LS} from "../../types/types"

const store ={favorites:[],
}

export default (state=store, action)=>{
 
  switch(action.type){
  case ADD_PRODUCTS_IN_FAVORITES:
    const product= state.favorites.find((productInCart)=> productInCart.article ==action.payload.article)
    const favorite = JSON.parse(localStorage.getItem('favorite'));
    const productInFavorite= favorite.find((favotiteItem)=> favotiteItem.article ==action.payload.article)
    //console.log(favorite)
  if(!product && !productInFavorite){
    //console.log(action.payload)
    let newFavorite= [...favorite,action.payload]
    //console.log(favorite)
    localStorage.setItem('favorite',JSON.stringify(newFavorite))
    return {
      ...state,
      favorites:[...state.favorites, action.payload]
    }
  } else{
    return {...state}
  } 
 
  case REMOVE_PRODUCTS_FROM_FAVORITES:
    const product_= state.favorites.find((productInCart)=> productInCart.article ==action.payload.article)
    const favorite_ = JSON.parse(localStorage.getItem('favorite'));
    console.log(favorite_)
    const productInFavorite_= favorite_.find((favotiteItem)=> favotiteItem.article ==action.payload.article)
    const newFavorite = [...favorite_.slice(0,favorite_.indexOf(productInFavorite_)),...favorite_.slice(favorite_.indexOf(productInFavorite_)+ 1)]
    console.log(newFavorite)
    //console.log(favorite_)
    localStorage.setItem('favorite',JSON.stringify([...favorite_.slice(0,favorite_.indexOf(productInFavorite_)),...favorite_.slice(favorite_.indexOf(productInFavorite_)+ 1)]))
    return {favorites:[...state.favorites.slice(0,state.favorites.indexOf(product_)),...state.favorites.slice(state.favorites.indexOf(product_)+ 1)]}

  case UPDATE_FROM_LS:
    //console.log(action.payload)
  return {...state, favorites:action.payload.favorite}
  
  default: 

  return state
 }
}
