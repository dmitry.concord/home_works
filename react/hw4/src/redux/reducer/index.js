import { combineReducers } from "redux";
import posts from './posts'
import favorite from "./favorite";
import productsInCart from "./productsInCart";



export const reducers = combineReducers({posts, favorite,productsInCart}) 