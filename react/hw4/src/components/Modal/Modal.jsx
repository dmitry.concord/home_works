import React from 'react';
import { addToFavorite } from '../../redux/actions/posts';
import Button from '../button/button';
import './modal.css'

const Modal = ({onClick,text,name,addItem}) => {
  return (
    <>
    <div className={'modal-wrapper'}>
        <div className="modal-header">
            <h2 className="modal-title">
                <p className="modal-text">{text}{name}? </p>
                <Button type="button" className={"close"} onClick={onClick} text={'X'}/>

            </h2>
        </div>
        <div className="modal-body">

        </div>
        <div className="button-container">

          <div className='button-container'>
                <Button text={'ok'}
                        className={'btn'}
                        onClick={addItem}
                       
                />
                <Button 
                        text={'deny'}
                        className={'btn'}
                        onClick={onClick}
                />
            </div> 
            

        </div>
    </div>
</>
  );
};

export default Modal;