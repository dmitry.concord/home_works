import React from 'react';
import { Route,Routes } from 'react-router-dom';
import Header from './components/header/Header';
import Favorites from './pages/favorites/Favorites';
import Home from './pages/Home/Home';
import CartPage from './pages/cartPage/CartPage';

const AppRouter = () => {
  return (<>
    
  <Header/>
  <Routes>
    <Route exact path='/' element={(
      <Home/>
    )
    }


    />

    <Route path='/favorites' element={

      <Favorites/>
    }/>

    <Route path='/incart' element={
   <CartPage/> }/>
    
  </Routes>
  </>
    
  );
};

export default AppRouter;