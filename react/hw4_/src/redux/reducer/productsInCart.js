import {ADD_PRODUCTS_TO_CART, REMOVE_PRODUCTS_FROM_CART,UPDATE_FROM_LS} from "../../types/types"

const store = {productsInCart:[],
}

export default (state=store, action)=>{
 
  switch(action.type){
  case ADD_PRODUCTS_TO_CART:
   const product= state.productsInCart.find((productInCart)=> productInCart.article ==action.payload.article)
  const cart = JSON.parse(localStorage.getItem('cart'));

  const productInCart= cart.find((productInCart)=> productInCart.article ==action.payload.article)
    //console.log(cart)
   // console.log(action.payload)
  if(!product && !productInCart){
    let newCart= [...cart,action.payload]
  console.log(newCart)
    localStorage.setItem('cart',JSON.stringify(newCart))
    return {
      ...state,
      productsInCart:[...state.productsInCart, action.payload]
    }

  }else{
    return {...state}
  }
  
  case REMOVE_PRODUCTS_FROM_CART:
  /* const product_= state.productsInCart.find((productInCart)=> productInCart.article ==action.payload.article);
  //console.log(product_)

  const cart_ = JSON.parse(localStorage.getItem('cart'));
  

  return {productsInCart:[...state.productsInCart.slice(0,state.productsInCart.indexOf(product_)),...state.productsInCart.slice(state.productsInCart.indexOf(product_)+ 1)]}
   */
  const product_= state.productsInCart.find((productInCart)=> productInCart.article ==action.payload.article);
 
  const cart_ = JSON.parse(localStorage.getItem('cart'));
 
  const productCart_= cart_.find((cartItem)=> cartItem.article ==action.payload.article)
  const newCart = [...cart_.slice(0,cart_.indexOf(productCart_)),...cart_.slice(cart_.indexOf(productCart_)+ 1)]
 
  localStorage.setItem('cart',JSON.stringify(newCart))
  return {productsInCart:[...state.productsInCart.slice(0,state.productsInCart.indexOf(product_)),...state.productsInCart.slice(state.productsInCart.indexOf(product_)+ 1)]}
  case UPDATE_FROM_LS:
    //console.log(action.payload)
  return {...state, productsInCart:action.payload.cart}

  default: 
  return state
 }
}