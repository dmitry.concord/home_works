import './header.css';
import logo from './logo.png'
import Main from '../main/Main';
import {Link} from 'react-router-dom'
import {Route,Routes} from 'react-router-dom'
import ProductList from '../product/ProductList';
import Header from './Header';
import Footer from '../footer/Footer';
import ProductCard from '../product/ProductCard';

      

function CartPage({products,handleModalOpen,addProductToFavorites,isAddedToFavorite,page,handleCartModal,deleteFromCart,deleteCartModal,handleCartItemModal,product}){
  let favorite = products.filter((product)=>product.isInFavorite == true)
  let cart =products.filter((product)=> product.isInCart==true)
  return(<>
<Header />
<div className="items-wrapper">
  <div className="items">
       {cart.map((product,index)=>( 
    
    <ProductCard 
      products={products}
      handleModalOpen={handleModalOpen}
      addToFavorite={addProductToFavorites}
      product={product}
      key={index}
      isAddedToFavorite={isAddedToFavorite}
      page={page}
      handleCartItemModal={handleCartItemModal}
      deleteFromCart={deleteFromCart}
    
    /> )
      
  )}    
</div>
</div>

{/* <Footer /> */}
  </>)
}


export default CartPage