import { Component } from "react";


class Button extends Component{


render(){
  const{text,onClick,className,style}= this.props;
  return (
      <button className={className} style={style} onClick={onClick}>{text}</button>
    )
  }
}

export default Button 