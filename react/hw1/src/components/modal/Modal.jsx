import { Component } from "react";
import Button from '../button/Button';
import './modal.css'


class Modal extends Component{

   constructor(props){
    super(props)
    console.log(this.props)
  }
 
  state= {
  closeButton:true,
  }


  render(){
  console.log(this.props)
   const {text,action,footer,header,className} = this.props
   
    return (
      <div className="wrapper" onClick= {action}>
      <div className={className} onClick={(e)=> e.stopPropagation()}>
          <div className="modal-header">
              <h2 className="modal-title">{header}
                   {this.state.closeButton && <button type="button" className="close" onClick={action}>
                  <span className="remove" aria-hidden="true">&times;</span>
                  </button>}  
              </h2>
          </div>
          <div className="modal-body">
          <p className="modal-text">{text}</p>
          </div>

          <p>{footer}</p>
          <div className="button-container">
      <Button text= {'ok'}  className={'btn'}/>
      <Button text= {'cancel'} className={'btn'}/>
      </div>
      </div>
      </div>
    )
  }

 /*  closeModal(){
    document.body.addEventListener('click', (event)=>{
      console.log(event.target)
    })
    
  } */


}

export default Modal