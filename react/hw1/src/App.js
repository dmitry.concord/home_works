//import logo from './logo.svg';
import { Component } from 'react';
import './App.css';
import Button from './components/button/Button';
import Modal from './components/modal/Modal'

class App extends Component{
constructor(props){
super(props)
this.closeModal()
}
  state= {
    isOpenModal1: false,
    isOpenModal2: false,
  }

 

  openFirstModal =()=>{
    this.setState({isOpenModal1: !this.state.isOpenModal1})
    this.setState({isOpenModal2: false})
    
    //document.body.style.backgroundColor ='black'; 
  }

  openSecondModal =()=>{
    this.setState({isOpenModal2: !this.state.isOpenModal2})
    this.setState({isOpenModal1: false})
  }

  render(){
    const{isOpenModal1,isOpenModal2} = this.state;
   
    return  (
      <>
      <div className='button-wrapper'>
      <Button text= {'button1'} className="btn" style={({backgroundColor: '#0099cc', width:'150px'})} onClick={this.openFirstModal}/>
      <Button text= {'button2'} className="btn" style={({backgroundColor: '#5555cc', width:'150px'})} onClick={this.openSecondModal}/>
      </div>
      {isOpenModal1 && <Modal text={'some text Modal1'}
      header={'modal1'}
      footer={'footer Modal1'}
      className={'modal active'}
      action = {this.openFirstModal}
      />}

      {isOpenModal2 && <Modal text={'some text Modal2'}
      header={'modal2'}
      footer={'footer Modal2'}
      className={'modal active'}
      action = {this.openSecondModal}
      />}
      </>
      
    )
  }

   closeModal(){
    document.body.addEventListener('click', (event)=>{
      if(this.state.isOpenModal1 != false || this.state.isOpenModal2 != false ){
        document.body.style.backgroundColor ='black';
      }else{
        document.body.style.backgroundColor ='white';
      }
      console.log(event.target.parentElement)
    })
    
  } 

}



export default App;
