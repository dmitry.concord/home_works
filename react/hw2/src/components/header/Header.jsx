import { Component } from 'react';
import { Icon } from '@iconify/react';
import './header.css'
/* import CartIcon from '../cartIcon/CartIcon'; */
import logo from './logo.png'



class Header extends Component{
constructor(props){
    super(props)
}
    render(){
        
        return(<> <header className='container'>
        {/* <CartIcon/> */}
        <div className='header-wrap'>
            <div className='logo'>
                <div className='logo-pic'><img src={logo} alt="logo" />
                </div>
                <p className="logo-text">BOOKSHOP</p>
            </div>
            <nav className='header-menu'>
                <ul className="header-menu-list">
                    <li className="menu-item"><a href="#">HOME</a></li>
                    <li className="menu-item"><a href="#">MAGAZINE</a></li>
                    <li className="menu-item"><a href="#">BLOG</a></li>
                    <li className="menu-item"><a href="#">NEWS</a></li>
                </ul>
            </nav>
            <div className="nav-search">  
            </div>
        </div>

        <section className="section-news">
            <div className="news-title">
                <p>THE LATEST NEWS ON DESIGN & ARCHITECTURE</p>
            </div>

            <div className='btn-wrap'>
                <button className='button left' type='button'>Subscribe Now</button>

                <button className='button right' type='button'>Best Articles</button>

            </div>
        </section>
    </header></>)
    }
}

export default Header