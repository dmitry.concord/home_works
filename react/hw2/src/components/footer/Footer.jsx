import { Component } from 'react';
import './footer.css'

function Footer(){
  return (
    <footer className="footer">
    <div className="container">
        © {new Date().getFullYear()} All rights reserved
    </div>
</footer>
  )
}

export default Footer