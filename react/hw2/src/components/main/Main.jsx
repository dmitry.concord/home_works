import { Component } from 'react';
import Header from '../header/Header';
/* import Content from '../content/Content'; */
import Footer from '../footer/Footer';
import CartIcon from '../cartIcon/CartIcon';
import Modal  from '../modal/Modal';
import ProductList from '../product/ProducList';
import CardModal from '../cardModal/CardModal';
import CartList from '../cartList/CartList';

 

class Main extends Component{
  constructor(props){
    super(props)
    this.state={products:[],
      isModalOpen:false,
      curentProductId:null,
      curentProduct:false,
      curentProductName:null,
      isAddedToFavorite:false,
      cardName:null,
      counter:null,
      cardModal:false,
      cartShow:false
    }
  }

  async getProduct(){
        try{
          const response =await fetch('./product.json')
          const data = await response.json()
          //console.log(data)
          const cart = JSON.parse(localStorage.getItem('cart'))
          const favorite =JSON.parse(localStorage.getItem('favorite'))
          const updateProducts = this.checkProducts(data,cart,favorite)
         
          console.log(updateProducts)
          this.setState({...this.state,products:updateProducts,counter:cart.length})
          return data
        }
        catch(error){
          console.log('error',error)
        }


  }
   checkProducts({data},cart,favorite){
      const updateProducts = data.map((product)=>{
      const productInCart = cart.some(({article})=>(article===product.article))
      console.log(productInCart)
      const productInFavorit = favorite.some(({article})=>{return(article===product.article)})
      //console.log(productInFavorit)
      return {...product,isInCart:productInCart, isInFavorite: productInFavorit}
      })
      return updateProducts
   }

   handleModal(id,name){
     console.log(id,name)
     if(id){
      // console.log(id,name)
       this.setState((state)=>({...state,isModalOpen:!state.isModalOpen,curentProductId:id,cardName:name}))
     } else{
      this.setState((state)=>({...state,isModalOpen:!state.isModalOpen,curentProductId:null,cardName:null}))
     }
     
   }

   addProductToCart(id,name){
     
    const cart = JSON.parse(localStorage.getItem('cart'));
    const product = this.state.products.find(product=>product.article==id)
    product.isInCart=true;
    
    const length =cart.push(product)
    localStorage.setItem('cart',JSON.stringify(cart))
    this.setState((state)=>({...state,isModalOpen:!state.isModalOpen,curentProductId:null,counter:length,cardModal:!this.state.cardModal})) 
    console.log(product)
   }

   addProductToFavorites(id){
    
    if(id ){
    const favorite = JSON.parse(localStorage.getItem('favorite'));
    
    const product = this.state.products.find(product=>{return product.article==id})
    product.isInFavorite=!product.isInFavorite
  
    //console.log(product.name)
    favorite.push(product)
    localStorage.setItem('favorite',JSON.stringify(favorite))
    this.setState((state)=>({...state,curentProductId:id,curentProduct:!state.curentProduct,isAddedToFavorite:!this.state.isAddedToFavorite}))
}
   }

   

  async componentDidMount(){
      if(!localStorage.getItem('cart')||!localStorage.getItem('favorite')){
        localStorage.setItem('cart',JSON.stringify([]))
        localStorage.setItem('favorite',JSON.stringify([]))
      }else{
        //console.log(JSON.parse(localStorage.getItem('favorite')))
      }

     await this.getProduct()
  }

   componentDidUpdate(){
    this.state.cardModal = false; 
    console.log('componentdidapdated') 
  } 

  showCart(){
   console.log('cart')
  /*  const curt = JSON.parse(localStorage.getItem('curt')); */
  this.setState((state)=>({...state,cartShow:!this.state.cartShow})) 
  }

  render(){
    const{isModalOpen,curentProductId,products,cardName,isAddedToFavorite,counter,cardModal,cartShow}=this.state
   // console.log(this.state)
  return(<>
    <CartIcon counter={counter}
      showCart={()=>this.showCart()}
    />
    <Header />

    <ProductList
      products={products}
      handleModalOpen={this.handleModal.bind(this)}
      addProductToFavorites={(id)=>this.addProductToFavorites(id)}
      isAddedToFavorite={isAddedToFavorite}
      
    />
    <Footer/>

    <Modal  isModalOpen={isModalOpen}
      onClose={()=>this.handleModal()}
      onSubmit={(id)=>this.addProductToCart(id,cardName)}
      id={curentProductId}
      name={cardName}
      text={`add to cart `}
      className={'modal-wrapper'}
    />
    <CardModal cardModal={cardModal}
      name={cardName}
    />
    <CartList cartShow={cartShow}/>


   {/*
   
   
   
   
   
    {this.state.isModalOpen && <Modal  isModalOpen={isModalOpen}
      onClose={()=>this.handleModal()}
      onSubmit={(id)=>this.addProductToCart(id)}
      id={curentProductId}
      text={'add to cart?'}
      className={'modal-wrapper'}
    />}  */}
    </>)
  }
}

export default Main