import { Component } from 'react';
import ProductCard from './ProductCard';



class ProductList extends Component{
  constructor(props){
    super(props)
  }

  render(){

   const{products,handleModalOpen,addProductToFavorites,isAddedToFavorite}=this.props
  return( <div className="items-wrapper">
  <div className="items">
  {products.map((product,index)=>( 
    
    <ProductCard 
      handleModalOpen={handleModalOpen}
      addToFavorite={addProductToFavorites}
      product={product}
      key={index}
      isAddedToFavorite={isAddedToFavorite}
    /> )
   
      
  )}
</div>
</div>
  ) 
  }
}

export default ProductList