import { Component } from 'react';
import Button from '../button/Button';
import IconStar from '../svg';
import './productCard.css'

class ProductCard extends Component{

constructor(props){
  super(props)

  
}
  render(){
    const{product,handleModalOpen,addToFavorite,isAddedToFavorite}=this.props;
    //console.log(this.props) 
   //console.log(product)
    return(<>
       <div className='card'>
         <IconStar  svgIsClicked={product.isInFavorite}
           isAddedToFavorite={isAddedToFavorite}
         />
        <div className='image-wrap'>
        <img className="fill" src={product.url} /> 
        </div>
        <div className='name'>Books name:{product.name}</div>
        <div className='price'>Price: {product.price}</div>  
        <div className='color'>Color:{product.color}</div>
        <div className="card-action">

        <Button text={'bay now'}
                className={'btn-small'}
                onClick={()=> {console.log(product.name)
                  handleModalOpen(product.article,product.name)}}
        />
               {/*  handleModalOpen(product.article) */}
        <Button text={'add to favorite'}
                className={'btn'} onClick={()=>{console.log('clic add to favorite')
                  addToFavorite(product.article)}}/>
        
        {/* addToFavorite(product.article) */}
               
         </div>
        </div> 
    </>)
  }
}

export default ProductCard