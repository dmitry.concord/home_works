import { Component } from "react";
import App from "../../App";

class Button extends Component{


render(){
  const{text,onClick,className,style,data_modal_id}= this.props;
  //console.log(this.props)
  return (
      <button data-id={data_modal_id} className={className} style={style} onClick={(event)=>{onClick(data_modal_id)}
        }>{text}</button>
    )
  }
}

export default Button 