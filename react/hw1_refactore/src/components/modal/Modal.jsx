import { Component } from "react";
import Button from '../button/Button';
import './modal.css'
import App from "../../App";


class Modal extends Component{
   constructor(props){
    super(props)
  }
 
  state= {
  closeButton:true,
  }


  render(){
   const {className,isOpenModal, modalData:{title,description},onClick,action} = this.props
   console.log(action)
  //console.log(modalData.title) 
   
    return (
      <> 
     {/*  {(isOpenModal && modalData )&& (<div className="wrapper" onClick={()=> onClick()}>
       <div className={className} onClick={(e)=> e.stopPropagation()}> 
          <div className="modal-header">
              <h2 className="modal-title">{modalData.title}
                    {this.state.closeButton && <button type="button" className="close" onClick={()=> onClick()} >
                  <span className="remove" aria-hidden="true">&times;</span>
                  </button>}  
              </h2>
          </div>
          <div className="modal-body">
          <p className="modal-text">{modalData.description}</p>
          </div>

          <p>{}</p>
          <div className="button-container">
      <Button text= {'ok'}  className={'btn'} />
      <Button text= {'cancel'} className={'btn'} />
      </div>
      </div>
      </div> 
      )} */}
       <div className={(isOpenModal&& title!=undefined)? "wrapper active" : "wrapper"} onClick={()=> onClick()}>
       <div className={className} onClick={(e)=> e.stopPropagation()}> 
          <div className="modal-header">
              <h2 className="modal-title">{title}
                    {this.state.closeButton && <button type="button" className="close" onClick={()=> onClick()} >
                  <span className="remove" aria-hidden="true">&times;</span>
                  </button>}  
              </h2>
          </div>
          <div className="modal-body">
          <p className="modal-text">{description}</p>
          </div>

          <p>{}</p>
          <div className="button-container">
      <Button text= {'ok'}  className={'btn'} />
      <Button text= {'cancel'} className={'btn'} />
      </div>
      </div>
      </div> 
      
      
      </>
     
    )
  }

 /*  closeModal(){
    document.body.addEventListener('click', (event)=>{
      console.log(event.target)
    })
    
  } */


}

export default Modal