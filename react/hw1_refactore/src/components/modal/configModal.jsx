import Button from "../button/Button"


const modalItems = [
  {
      id: 1,
      title: "title for modal 1",
      description: 'description for modal 1',
      isActive:false,
      hasCloseButton:true,
      className:'modal active',
      hideFn:(id)=> this.hideModalId(id),
      actions: [
        <Button text='Ok' backgroundColor='#008CBA' onClick={() => {
          alert('Done!');
          this.hideModal(0);
        }} />,
        <Button text='Cancel' backgroundColor='#E7E7E7' onClick={() => {
          alert('Canceled!');
          this.hideModal(0);
        }} />
      ],

  },
  {
      id: 2,
      title: "title for modal 2",
      description: 'description for modal 2',
      isActive:false,
      hasCloseButton:false,
      className:'modal active',
      hideFn:(id)=> this.hideModalId(id),
      actions: [
        
        <Button text='Ok' backgroundColor='#008CBA' onClick={() => {
          alert('Done!');
          this.hideModal(0);
        }} />,
        <Button text='Cancel' backgroundColor='#E7E7E7' onClick={() => {
          alert('Canceled!');
          this.hideModal(0);
        }} />
      ],
      
  }
]

export default modalItems


/* import modalItems from './configModal';

 const getModalId = (id) => {
  console.log(id)
  

  let modalId = modalItems.find(modalItem =>{if(modalItem.id=== id ){
    return modalItem
  }
 
  })
  
  return modalId
  }
 
export default getModalId */



/* import { Component } from 'react';
import './App.css';
import Button from './components/button/Button';
import Modal from './components/modal/Modal'
import getModalId from './components/modal/getModalContent';
import modalItems from './components/modal/configModal';

class App extends Component{
constructor(props){
super(props)

}
  state= {
  //isOpenModal: false,
  //modalData:{title:'',description:''}
  modals:modalItems
  }

 

 

  showModal(id){
    this.setState((curent)=>{
      const newState ={...curent}
      newState.modals[id].isActive=true
      return newState
    })
  }

  hideModal(id){
    this.setState((curent)=>{
      const newState ={...curent}
      newState.modals[id].isActive=false
      return newState
    })
  }

  render(){
  const{isOpenModal,modalData} = this.state;
  return  (
      <>
      <div className='button-wrapper'>
      <Button text= {'oppen modal 1'}
       className="btn" 
       style={({backgroundColor: '#0099cc', width:'150px'})} 
       onClick={this.openModal}
       data_modal_id={1}
       />
      <Button text= {'oppen modal 2'} 
       className="btn" 
       style={({backgroundColor: '#5555cc', width:'150px'})} 
       onClick={this.openModal}
       data_modal_id={2}
      />
      </div>
      
           {modalItems[0].isActive && <Modal {...modalItems[0]}/>}
           {modalItems[1].isActive && <Modal {...modalItems[1]}/>}
      </>
      
    )
  }


}



export default App; */



/* import { Component } from "react";
//import Button from '../button/Button';
import './modal.css'
import App from "../../App";


class Modal extends Component{
  

  render(){
   const {id,title,description,hasCloseButton,hideFn,actions,className} = this.props
   
   
    return (
      <> 
   
       <div className={(this.isOpenModal&& title!=undefined)? "wrapper active" : "wrapper"} onClick={()=> hideFn(id)}>
       <div className={className} onClick={(e)=> e.stopPropagation()}> 
          <div className="modal-header">
              <h2 className="modal-title">{title}
                    {hasCloseButton && <button type="button" className="close" onClick={()=> hideFn(id)} >
                  <span className="remove" aria-hidden="true">&times;</span>
                  </button>}  
              </h2>
          </div>
          <div className="modal-body">
          <p className="modal-text">{description}</p>
          </div>

          <p>{}</p>
           <div className="button-container">
       {actions?.length>0&&actions[0]} 
       {actions?.length>1&&actions[1]} 
      </div> 
      </div>
      </div> 
      
      
      </>
     
    )
  }

 


}

export default Modal */



/* 
import { Component } from "react";
import App from "../../App";

class Button extends Component{


render(){
  const{text,onClick,className,style,data_modal_id}= this.props;
  //console.log(this.props)
  return (
      <button data-id={data_modal_id} className={className} style={style} onClick={(event)=>{onClick(data_modal_id)}
        }>{text}</button>
    )
  }
}

export default Button  */