 const getModalId = (id) => {
  const modalItems = [
      {
          id: 1,
          title: "title for modal 1",
          description: 'description for modal 1'
      },
      {
          id: 2,
          title: "title for modal 2",
          description: 'description for modal 2'
      }
  ]

  let modalId = modalItems.find(modalItem =>{if(modalItem.id=== id ){
    return modalItem
  }
 
  })
  
  return modalId
  }
 
export default getModalId