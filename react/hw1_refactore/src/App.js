import { Component } from 'react';
import './App.css';
import Button from './components/button/Button';
import Modal from './components/modal/Modal'
import getModalId from './components/modal/getModalContent';

class App extends Component{
constructor(props){
super(props)

}
  state= {
  isOpenModal: false,
  modalData:{title:'',description:''}
  }

 

 openModal =(id)=>{
    const modalData =getModalId(id)
    console.log(modalData)
    if(modalData){
        this.setState({isOpenModal: !this.state.isOpenModal, modalData:modalData })
      }else{
        this.setState({isOpenModal: !this.state.isOpenModal, modalData:{} })
      }
    
  } 

  componentDidUpdate(){
    this.state.isOpenModal = false; 
  }

  render(){
  const{isOpenModal,modalData} = this.state;
  return  (
      <>
      <div className='button-wrapper'>
      <Button text= {'oppen modal 1'}
       className="btn" 
       style={({backgroundColor: '#0099cc', width:'150px'})} 
       onClick={this.openModal}
       data_modal_id={1}
       />
      <Button text= {'oppen modal 2'} 
       className="btn" 
       style={({backgroundColor: '#5555cc', width:'150px'})} 
       onClick={this.openModal}
       data_modal_id={2}
      />
      </div>
      <Modal isOpenModal={isOpenModal}
        modalData={modalData}
        className={'modal active'}
        onClick={this.openModal}
        action={<><Button/></>}
      />
           
      </>
      
    )
  }


}



export default App;

