import React, { useState } from 'react';
import Modal from "./Modal";
import {render, screen} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Provider} from 'react-redux';
import { legacy_createStore as createStore } from 'redux';
import {reducers} from '../../redux/reducer/index';
import FavoriteCard from "../favoriteCards/favoriteCard";
import ProductCard from "../productCard/productCard"

describe('Modal component', () => {
 
  const state = {favoriteModal:false,
    addToCartModal:true,
    deleteFormFavorite:true,
    deleteFromCart:true,
    addToFavorite:true,
  }
  let item = {name:"Код Давинчи",price:"200",url:"/img/braun.jpg","color":"крассный","article":"e2203"}
 

  describe('<Modal/> component',()=>{
   
    if(state.deleteFormFavorite){
    
     it('should delete to favorite Modal', ()=>{
      let item = {name:"Код Давинчи",price:"200",url:"/img/braun.jpg","color":"крассный","article":"e2203"}
      const handleClick = jest.fn((e)=>console.log('deleteFromFavorite'))
      const {container} = render(<Modal name={'Код Давинчи'} text={'Do you want to delete from Favorite'}  addItem={handleClick} /> )
      let hasText = container.innerHTML.includes('Do you want to delete from Favorite')
      expect(hasText).toBe(true) 
      
  }) 

   
 

  it('should render without content', ()=>{
    const {container} = render(<Modal/> )
    expect(screen.queryByRole('Modal')).toBeNull()
  })
    }
  })
  if(state.addToCartModal){
    
   
 it('should add to cart Modal', ()=>{
  let item = {name:"Код Давинчи",price:"200",url:"/img/braun.jpg","color":"крассный","article":"e2203"}
  const handleClick = jest.fn((e)=>console.log('addToFavorite'))
  const {container} = render(<Modal name={'Код Давинчи'} text={'Do you want to add to Favorite'}  addItem={handleClick} /> )
  
  let hasText = container.innerHTML.includes('Do you want to add to Favorite')
  userEvent.type(screen.getByText('ok'), "React")

   expect(handleClick).toHaveBeenCalled()
  
}) 

it('should pass onClick if modal button is clicked', ()=>{
  let item = {name:"Код Давинчи",price:"200",url:"/img/braun.jpg","color":"крассный","article":"e2203"}
  const handleClick = jest.fn((e)=>console.log('modal button was clicked to addToFavorite'))
  const {container} = render(<Modal name={'Код Давинчи'} text={'Do you want to add to Cart'}  addItem={handleClick} /> )
  
  let hasText = container.innerHTML.includes('Do you want to add to Cart')
  userEvent.type(screen.getByText('ok'), "React")
   expect(hasText).toBe(true) 

}) 


   }
   if(state.addToFavorite){
    
    it('should add to favorite Modal', ()=>{
     let item = {name:"Код Давинчи",price:"200",url:"/img/braun.jpg","color":"крассный","article":"e2203"}
     const handleClick = jest.fn((e)=>console.log('addToFavorite'))
     const {container} = render(<Modal name={'Код Давинчи'} text={'Do you want to add to Favorite'}  addItem={handleClick} /> )
     
     let hasText = container.innerHTML.includes('Do you want to add to Favorite')
     
     expect(hasText).toBe(true) 
   
    
 }) 

 
   }

   if(state.deleteFromCart){
    
    it('should delete from cart Modal', ()=>{
     let item = {name:"Код Давинчи",price:"200",url:"/img/braun.jpg","color":"крассный","article":"e2203"}
     const handleClick = jest.fn((e)=>console.log('addToFavorite'))
     const {container} = render(<Modal name={'Код Давинчи'} text={'Do you want to delete from cart'}  addItem={handleClick} /> )
     
     let hasText = container.innerHTML.includes('Do you want to delete from cart')
    
     expect(hasText).toBe(true) 
   
    
 }) 

   }

})
