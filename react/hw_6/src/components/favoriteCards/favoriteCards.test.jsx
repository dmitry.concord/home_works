import {render, screen} from "@testing-library/react";
import FavoriteCard from "./favoriteCard"
import {Provider} from "react-redux";
import {legacy_createStore as createStore} from "redux";
import {reducers} from "../../redux/reducer/index"

describe('<FavoriteCard/> component', ()=>{
  it('should render with favorite items', ()=>{
    const store=createStore(reducers,{favorites:[{name:"Код Давинчи",price:"200",url:"/img/braun.jpg","color":"крассный","article":"e2203"}]})
    render(<Provider store={store}>
      <FavoriteCard item={{name:"Код Давинчи",price:"200",url:"/img/braun.jpg","color":"крассный","article":"e2203"}}/>
    </Provider>)
    screen.debug()
    screen.getByText(/Код Давинчи/)
  })
})