import { useDispatch,useSelector } from 'react-redux';
import {useState,useEffect,} from 'react';
import { Icon } from '@iconify/react';
import "./cartIcon.css"


function CartIcon({showCart,cartLenght}){

  const dispatch = useDispatch()
  
  useEffect(()=>{
    let cart = JSON.parse(localStorage.getItem('cart'))
    
    cart? cart =cart : cart=[]
  
  },[])
  
  
    return(<div className='cart-icon' onClick={showCart}>
    <Icon icon="emojione:shopping-cart" width="40" height="40" />  
    <span className='quantity'>{cartLenght}</span> 
   
    </div>)
  }
  
  export default CartIcon