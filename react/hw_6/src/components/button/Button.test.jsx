import {render, screen} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Button from './button'


describe('<Button/> component', ()=>{
  it('should render button Tag', ()=>{
  const {container} = render(<Button text={'Add to Fav'} className={'button'}/> )
    screen.debug()

  const button = container.querySelector('.button')
  expect(button).not.toBeNull()
  })
  it('should render button with text', ()=>{
    const {container} = render(<Button text={'Add to Fav'} className={'button'}/>)
   
    const button = container.querySelector('.button')
    screen.getByRole('button')
    let hasText = button.innerHTML.includes('Add to Fav')
    expect(hasText).toBe(true) 
  }) 
    it('should pass onClick if button is clicked', ()=>{
      const handleClick =jest.fn()
      const {container} = render(<Button text={'Add to Fav'} className={'button'} onClick={handleClick}/>)
      userEvent.click(screen.getByRole('button'))
      expect(handleClick).toHaveBeenCalled()
    })
})

