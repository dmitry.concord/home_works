import React from 'react';

const Button = ({className,onClick,text,id}) => {

  return (
    <button id="id"  className={className} onClick={onClick}>{text}</button>
  );
};

export default Button;