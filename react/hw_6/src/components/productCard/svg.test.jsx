import {render, screen} from "@testing-library/react";
import IconStar from "./svg";

describe('<IconStar/> component',()=>{
  it('should render', ()=>{
    const {container} = render(<IconStar />)
    expect(container.firstChild).toMatchSnapshot()
  },)
})