import productInCartReducer from "./productsInCart";
import {addNewItemToCart, deleteExistItem} from "./productsInCart";

describe('add to cart', ()=> {
  let state= {productsInCart:[{name:"Вино из одуванчиков",article:"e2204"}]
   
  }

  it('should return default value', ()=>{
    
    expect((undefined,[])).toEqual([])
  })

  it("should not to add to cart", ()=> {
    jest.spyOn(Storage.prototype,'getItem');
    Storage.prototype.getItem = jest.fn((cart)=> [{name:"Вино из одуванчиков",article:"e2204"}])
    jest.spyOn(JSON,'parse').mockReturnValue([{name:"Вино из одуванчиков",article:"e2204"}])
   
    expect(productInCartReducer(state, addNewItemToCart({name:"Вино из одуванчиков",article:"e2204"}))).toEqual({productsInCart:[{name:"Вино из одуванчиков",article:"e2204"}]})
  }
  )

   it('should to add to cart', ()=>{
    jest.spyOn(Storage.prototype,'getItem');
    Storage.prototype.getItem = jest.fn((cart)=> [{name:"Вино из одуванчиков",article:"e2204"}])
    jest.spyOn(JSON,'parse').mockReturnValue([{name:"Вино из одуванчиков",article:"e2204"}])

    expect(productInCartReducer(state, addNewItemToCart({name: '1984',article:"f365"}))).toEqual({productsInCart:[...state.productsInCart,{name: '1984',article:"f365"}]})
  } 
  )
  
})

 describe('remove from cart', ()=> {
  let state = {productsInCart:[{name:"234",article:"e2206"},{name:"Вино из одуванчиков",article:"e2204"}]}
  jest.spyOn(JSON,'parse').mockReturnValue([{name:"234",article:"e2206"},{name:"Вино из одуванчиков",article:"e2204"}])
  Storage.prototype.getItem = jest.fn((favorites)=> [{name:"234",article:"e2206"},{name:"Вино из одуванчиков",article:"e2204"}])
  
  it('should delete value', ()=>{
  
  expect(productInCartReducer(state,deleteExistItem({name:"234",article:"e2206"}))).toEqual({productsInCart:[{name:"Вино из одуванчиков",article:"e2204"}]})
  })


}) 