import favoriteReducer from "./favorite";
import{addNewItemCreator, deleteExistItem} from './favorite'

describe('add to favorits', () => {

  let state= {favorites:[{name:"234",article:"e2206"}]}
 
  it('should return default value', ()=>{
    
    expect((undefined,[])).toEqual([])
  })


  it("should add favorite", ()=> {
    expect(favoriteReducer(state, addNewItemCreator({name:"Вино из одуванчиков",article:"e2204"}))).toEqual({favorites:[{name:"234",article:"e2206"},{name:"Вино из одуванчиков",article:"e2204"}]})
  }
  )
  
  it("should not to add duplicated favorite", ()=> {
    jest.spyOn(Storage.prototype,'getItem');
    Storage.prototype.getItem = jest.fn((favorites)=> [{name:"234",article:"e2206"},{name:"Вино из одуванчиков",article:"e2204"}])
    jest.spyOn(JSON,'parse').mockReturnValue([])

    expect(favoriteReducer(state, addNewItemCreator({name:"Вино из одуванчиков",article:"e2204"}))).toEqual({favorites:[{name:"234",article:"e2206"},{name:"Вино из одуванчиков",article:"e2204"}]}) 
  })

});

describe('remove from favorite', ()=> {
  let state= {favorites:[{name:"234",article:"e2206"},{name:"Вино из одуванчиков",article:"e2204"}]}
  jest.spyOn(JSON,'parse').mockReturnValue([{name:"234",article:"e2206"},{name:"Вино из одуванчиков",article:"e2204"}])
  Storage.prototype.getItem = jest.fn((favorites)=> [{name:"234",article:"e2206"},{name:"Вино из одуванчиков",article:"e2204"}])
  
  it('should delete value', ()=>{
  
    expect(favoriteReducer(state,deleteExistItem({name:"234",article:"e2206"}))).toEqual({favorites:[{name:"Вино из одуванчиков",article:"e2204"}]})
  })

 

})