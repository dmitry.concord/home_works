
import {ADD_PRODUCTS_IN_FAVORITES,REMOVE_PRODUCTS_FROM_FAVORITES,UPDATE_FROM_LS} from "../../types/types"

const store ={favorites:[],
}

const favoriteReducer = (state=store, action)=>{
 
  switch(action.type){


  case ADD_PRODUCTS_IN_FAVORITES:
    const product= state.favorites.find((productInCart)=> productInCart.article ==action.payload.article)

    let favorite = JSON.parse(localStorage.getItem('favorite'));
    
    !favorite ? favorite = [] : favorite = favorite
    let productInFavorite= favorite.find((favotiteItem)=> favotiteItem.article ==action.payload.article)
    
  if(!product && !productInFavorite){
  
    let newFavorite= [...favorite,action.payload]
    
    localStorage.setItem('favorite',JSON.stringify(newFavorite))
    return {
      ...state,
      favorites:[...state.favorites, action.payload]
    }
  } else{
    return {...state}
  } 

 
  case REMOVE_PRODUCTS_FROM_FAVORITES:
    
    let product_= state.favorites.find((productInCart)=> productInCart.article ==action.payload.article)
   // 
    let favorite_ = JSON.parse(localStorage.getItem('favorite'));
    !favorite_ ? favorite_ = [] : favorite_ = favorite_
    let productInFavorite_= favorite_.find((favotiteItem)=> favotiteItem.article ==action.payload.article)
    !productInFavorite_ ? productInFavorite_ = [] : productInFavorite_ = productInFavorite_
    const newFavorite = [...favorite_.slice(0,favorite_.indexOf(productInFavorite_)),...favorite_.slice(favorite_.indexOf(productInFavorite_)+ 1)]
    console.log(newFavorite)
    
    localStorage.setItem('favorite',JSON.stringify([...favorite_.slice(0,favorite_.indexOf(productInFavorite_)),...favorite_.slice(favorite_.indexOf(productInFavorite_)+ 1)]))
    return {favorites:[...state.favorites.slice(0,state.favorites.indexOf(product_)),...state.favorites.slice(state.favorites.indexOf(product_)+ 1)]}

  case UPDATE_FROM_LS:
    
  return {...state, favorites:action.payload.favorite}
  
  default: 

  return state
 }

}
export const addNewItemCreator = payload => (store,{type: ADD_PRODUCTS_IN_FAVORITES, payload});
export const deleteExistItem = payload => (store,{type:REMOVE_PRODUCTS_FROM_FAVORITES, payload})
export default favoriteReducer

