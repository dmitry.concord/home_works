import ProductCard from "./ProductCard";
import Main from "../main/Main";
import FavPage from "../header/FavPage";
import CartPage from "../header/CartPage"

function ProductList({products,handleModalOpen,addProductToFavorites,isAddedToFavorite,page,openModal,data_modal_id,deleteFromCart,deleteCartModal,handleCartItemModal,product}){

  if(products){}
 let favorite = products.filter((product)=>product.isInFavorite == true)
 let cart =products.filter((product)=> product.isInCart==true)

  let mainContent;
  page == 'home' && (mainContent = (<div className="items-wrapper">
  <div className="items">
       {products.map((product,index)=>( 
    <ProductCard 
      handleModalOpen={handleModalOpen}
      addToFavorite={addProductToFavorites}
      product={product}
      key={index}
      isAddedToFavorite={isAddedToFavorite}
      page={page}
     
    /> )
      
  )}    
</div>
</div>
  ) )
  
  page == 'favorites' && (mainContent = ( <div className="items-wrapper">
  <div className="items">
       {favorite.map((product,index)=>( 
    
    <ProductCard 
      handleModalOpen={handleModalOpen}
      addToFavorite={addProductToFavorites}
      product={product}
      key={index}
      isAddedToFavorite={isAddedToFavorite}
      page={page}
    /> )
      
  )}    
</div>
</div>
  ) ) 

  page == 'incart' && (mainContent = ( <div className="items-wrapper">
  <div className="items">
       {cart.map((product,index)=>
       ( 
    
    <ProductCard 
      handleModalOpen={handleModalOpen}
      addToFavorite={addProductToFavorites}
      product={product}
      key={index}
      isAddedToFavorite={isAddedToFavorite}
      page={page}
      openModal={openModal}
      deleteFromCart={deleteFromCart}
      handleCartItemModal={handleCartItemModal}
      deleteCartModal={deleteCartModal}
      
    /> )
      
  )}    
</div>
</div>
  ) )
  return mainContent
}

export default ProductList