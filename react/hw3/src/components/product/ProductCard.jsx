import {useState} from "react";
import Button from "../button/Button";
import IconStar from './svg';
import './productCard.css';
import { Icon } from '@iconify/react';
import Modal from "../modal/Modal";




function ProductCard({product,handleModalOpen,addToFavorite,isAddedToFavorite,page,deleteFromCart,handleCartItemModal,deleteCartModal,}){
  const [isShown, setIsShown] = useState(false)
  const handler = () => {
      setIsShown(!isShown) 
    }

  let card;
  page=='incart' && (card = (<>
    <div className='card'>
     <IconStar  svgIsClicked={product.isInFavorite}
        
      />
     <div className="delete-btn">
                                                 
         <Icon icon="emojione-monotone:cross-mark-button" width="30" height="30" onClick={()=>{handleCartItemModal(product.article)
         console.log('click delete card')}
         }/>

     </div>
     <div className='image-wrap'>
     <img className="fill" src={product.url} />
     </div>
     <div className='name'>Books name:{product.name}</div>
     <div className='price'>Price: {product.price}</div>
     <div className='color'>Color:{product.color}</div>
     <div className="card-action">
     

     <Button text={'bay now'}
             className={'btn-small'}
             onClick={()=> {console.log(product.name)
              handleModalOpen(product.article,product.name)
              }}
     />
          
     <Button text={'add to favorite'}
             className={'btn'} onClick={()=> addToFavorite(product.article)}/>
         
      </div>
      
     </div>
  </>))
    page=='favorites' && (card = (<>
    <div className='card'>
    <IconStar  svgIsClicked={product.isInFavorite}/>
       <div className="delete-btn" >{(page=='inCart' || page =='favorites')&&  page=='favorites' &&(<Icon icon="emojione-monotone:cross-mark-button" width="30" height="30" onClick={()=>addToFavorite(product.article)}/>)}</div>

     <div className='image-wrap'>
     <img className="fill" src={product.url} />
     </div>
     <div className='name'>Books name:{product.name}</div>
     <div className='price'>Price: {product.price}</div>
     <div className='color'>Color:{product.color}</div>
     <div className="card-action">

     <Button text={'bay now'}
             className={'btn-small'}
             onClick={()=> {console.log(product.name)
               handleModalOpen(product.article,product.name)}}
     />
           
     <Button text={`delete from favorite`}
             className={'btn'} onClick={()=>
     {console.log('clic add to favorite')
         handler()

     }}/>
         {isShown && <Modal
        text={`do you want to delete from favorite ${product.name}`}
             closeText={'No i dont want to do it'}
             submitText={'Yes, please'}
             onClose={handler}
             onSubmit={addToFavorite}
             isShown={isShown}
             product={product}
         /> }
      </div>
     </div>
  </>))

  page == 'home' && (card = (<>
    <div className='card'>
    <IconStar  svgIsClicked={product.isInFavorite}
      
      />

     <div className='image-wrap'>
     <img className="fill" src={product.url} />
     </div>
     <div className='name'>Books name:{product.name}</div>
     <div className='price'>Price: {product.price}</div>
     <div className='color'>Color:{product.color}</div>
     <div className="card-action">

     <Button text={'bay now'}
             className={'btn-small'}
             onClick={()=> {console.log(product.name)
               handleModalOpen(product.article,product.name)}}
     />
     <Button text={`add to favorite`}
             className={'btn'} onClick={()=>
     {console.log('clic add to favorite')
               addToFavorite(product.article)
         handler()

     }}/>
       
      </div>
     </div>
  </>))
return card
}

export default ProductCard
