import Button from "../button/Button";
import "./modal.css"


function Modal({isShown, className, isModalOpen, onClose, onSubmit, id, name, togleModal, submitText, closeText,text,onAdmit,product,deleteCartModal,deleteFromCart}) {
    
    return (
        <>
        <div className={'modal-wrapper'}>
            <div className="modal-header">
                <h2 className="modal-title">
                    <p className="modal-text">{text}{name}? </p>
                    <Button type="button" className={"close"} onClick={onClose} text={'X'}/>

                </h2>
            </div>
            <div className="modal-body">

            </div>
            <div className="button-container">

              {isModalOpen &&(<div className='button-container'>
                    <Button text={submitText}
                            className={'btn'}
                            onClick={() => onSubmit(id, name)}
                    
                    />
                    <Button text={closeText}
                            className={'btn'}
                            onClick={onClose}
                    />
                </div>)}  
                {isShown && (<div className='button-container'>
                    <Button text={submitText}
                            className={'btn'}
                            onClick={()=> {onSubmit(product.article) 
                            setTimeout(() => onClose(), 100); 
                            }}
                            
                    />
                    <Button text={closeText}
                            className={'btn'}
                            onClick={onClose}
                    />
                </div>)}

                 {deleteCartModal && (<div className='button-container'>
                    <Button text={`delete from Cart ${name} `}
                            className={'btn'}
                            onClick={()=> {deleteFromCart(id,name)
                            onClose()
                            }
                                }
                            
                    />
                    <Button text={'deny delete from Cart'}
                            className={'btn'}
                            onClick={onClose}
                    />
                </div>)} 

            </div>
        </div>
    </>
    )


}

export default Modal
