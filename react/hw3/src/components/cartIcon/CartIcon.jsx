import { Icon } from '@iconify/react';
import "./cartIcon.css"


function CartIcon({counter,showCart}){
//console.log(counter)
  return(<div className='cart-icon' onClick={showCart}>
  <Icon icon="emojione:shopping-cart" width="40" height="40" />  
  <span className='quantity'>{counter}</span> 
 {/*  {props.length ? <span>{props.length}</span> : null} */}
  </div>)
}

export default CartIcon