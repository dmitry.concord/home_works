import './header.css';
import logo from './logo.png'
import Main from '../main/Main';
import {Link} from 'react-router-dom'
import {Route,Routes} from 'react-router-dom'
import ProductList from '../product/ProductList';
import Header from './Header';
import Footer from '../footer/Footer';
import ProductCard from '../product/ProductCard';


function FavPage({products,handleModalOpen,addProductToFavorites,isAddedToFavorite,page}){

  let favorite = products.filter((product)=>product.isInFavorite == true)
  let cart =products.filter((product)=> product.isInCart==true)
  return(<>
<Header />
<div className="items-wrapper">
  <div className="items">
       {favorite.map((product,index)=>( 
    
    <ProductCard 
      handleModalOpen={handleModalOpen}
      addToFavorite={addProductToFavorites}
      product={product}
      key={index}
      isAddedToFavorite={isAddedToFavorite}
      page={page}
    /> )
      
  )}    
</div>
</div>

{/* <Footer /> */}
  </>)
}


export default FavPage