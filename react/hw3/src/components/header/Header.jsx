import './header.css';
import logo from './logo.png'
import Main from '../main/Main';
import {Link,useNavigate} from 'react-router-dom'
import {Route,Routes} from 'react-router-dom'
import ProductList from '../product/ProductList';

function Header({onLinkClick,page}){
const navigate= useNavigate()
let header;
const handleClick=(event,pageName)=>{
event.preventDefault()
onLinkClick(pageName)
navigate(`/${pageName}`)
console.log(pageName)
}

page == 'home' && (header = (<> <header className='container'>
<div className='header-wrap'>
    <div className='logo'>
         <div className='logo-pic'><img src={logo} alt="logo" />
        </div> 
        <p className="logo-text">BOOKSHOP</p>
    </div>
    <nav className='header-menu'>
        <ul className="header-menu-list">
         <li className="menu-item"><Link className='link'  onClick={(event)=> handleClick(event,'home')} to="/home">HOME</Link></li>
         <li className="menu-item"><Link className='link' onClick={(event)=> handleClick(event,'favorites')} to="/favorites">FAVORITES</Link></li>
         <li className="menu-item"><Link className='link' onClick={(event)=> handleClick(event,'incart')} to="/inCart">IN Cart</Link></li>
         <li className="menu-item"><a href="#">NEWS</a></li>
        </ul>
    </nav>
    <div className="nav-search">  
    </div>
</div>

<section className="section-news">
    <div className="news-title">
        <p>THE LATEST NEWS ON DESIGN & ARCHITECTURE</p>
    </div>

    <div className='btn-wrap'>
        <button className='button left' type='button'>Subscribe Now</button>

        <button className='button right' type='button'>Best Articles</button>

    </div>
</section>
</header></>))

page == 'favorites' && (header = (<> <header className='container'>
<div className='header-wrap'>
    <div className='logo'>
         <div className='logo-pic'><img src={logo} alt="logo" />
        </div> 
        <p className="logo-text">YOURE FAVORITE BOOKS</p>
    </div>
    <nav className='header-menu'>
        <ul className="header-menu-list">
         <li className="menu-item"><Link className='link' onClick={(event)=>handleClick(event,'home')} to="/home">HOME</Link></li>
         <li className="menu-item"><Link className='link' onClick={(event)=> handleClick(event,'favorites')} to="/favorites">FAVORITES</Link></li>
         <li className="menu-item"><Link className='link' onClick={(event)=> handleClick(event,'incart')} to="/inCart">IN Cart</Link></li>
         <li className="menu-item"><a href="#">NEWS</a></li>
        </ul>
    </nav>
    <div className="nav-search">  
    </div>
</div>

</header></>))

page == 'incart' && (header = (<> <header className='container'>
<div className='header-wrap'>
    <div className='logo'>
         <div className='logo-pic'><img src={logo} alt="logo" />
        </div> 
        <p className="logo-text">BOOKS IN CART</p>
    </div>
    <nav className='header-menu'>
        <ul className="header-menu-list">
            
         <li className="menu-item"><Link className='link' onClick={(event)=>handleClick(event,'home')} to="/home">HOME</Link></li>
         <li className="menu-item"><Link className='link' onClick={(event)=> handleClick(event,'favorites')} to="/favorites">FAVORITES</Link></li>
         <li className="menu-item"><Link className='link' onClick={(event)=> handleClick(event,'incart')} to="/inCart">IN Cart</Link></li>
         <li className="menu-item"><a href="#">NEWS</a></li>
        </ul>
    </nav>
    <div className="nav-search">  
    </div>
</div>

</header></>))
return header
}

export default Header