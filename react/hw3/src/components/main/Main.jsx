import {useState,useEffect} from 'react';
import GetProduct from './GetProduct';
import Header from "../header/Header"
import CartIcon from '../cartIcon/CartIcon';
import Footer from '../footer/Footer';
import ProductList from '../product/ProductList';
import Modal from '../modal/Modal';
import CardModal from '../cardModal/CardModal';
import {Route,Routes} from 'react-router-dom';
import FavPage from '../header/FavPage';
import CartPage from '../header/CartPage';
import HomePage from '../header/HomePage';






function Main(props){

  const[products,setProduct]= useState([])
  const[isModalOpen,setIsModalOpen] =useState(false)
  const[curentProductId,setCurrentProductId] =useState(null)
  const[curentProduct,setCurrentProduct]=useState(false)
  const[currentProductName,setCurrentProductName]=useState(null)
  const[isAddedToFavorite,setIsAddedToFavorite]=useState(false)
  const[cardName,setToCardName]=useState(null)
  const[counter,setCounter]=useState(null)
  const[cardModal,setCardModal]=useState(false)
  const[cartShow,setCartShow]=useState(false)

  const [cartItems, setCartItems] = useState([]);
  const [page, setPage] =useState('home')


  const[deleteCartModal,setDeleteCartModal] = useState(false)

  const checkProducts =(data,cart,favorite)=>{
  const updateProducts = data.map((product)=>{
  const productInCart = cart.some(({article})=>(article===product.article))
  const productInFavorit = favorite.some(({article})=>(article===product.article))

    return {...product,isInCart:productInCart, isInFavorite: productInFavorit, quantity:1}
    })
    return updateProducts
 }


 useEffect(()=>{

  if(!localStorage.getItem('cart')||!localStorage.getItem('favorite')){
//console.log('локалсторедж нет')
  localStorage.setItem('cart',JSON.stringify([]))
  localStorage.setItem('favorite',JSON.stringify([]))
  }else{
    //console.log('локалсторедж есть')
  }

  const items = GetProduct()
  .then(product=>{
  const cart = JSON.parse(localStorage.getItem('cart'))
  const favorite =JSON.parse(localStorage.getItem('favorite'))
  const updateProducts = checkProducts(product,cart,favorite)
  setProduct(updateProducts)
  const length = cart.length;
  setCounter(length)
  })

 },[])

 useEffect(()=>{
 setTimeout(() => setCardModal(false), 1000);
 },[cardModal])


  const handleModal=(id,name)=>{
  if(id){
    setIsModalOpen(!isModalOpen)
    setCurrentProductId(id)
    setToCardName(name)

  } else{
    setIsModalOpen(false)
    setCurrentProductId(null)
    setToCardName(null)
  }

 }

const addProductToCart =(id,name,quantity=1)=>{

let cart = JSON.parse(localStorage.getItem('cart'));
let favorite =JSON.parse(localStorage.getItem('favorite'))
const product = products.find(item=>item.article==id)

if(cart.length>0){
const elems = cart.find((cartItem)=> cartItem.article == product.article)

elems ? (elems.quantity +=1):(cart.push(product))  
localStorage.setItem('cart',JSON.stringify(cart))
let updataItems= checkProducts(products,cart,favorite)
console.log(products)
setProduct(updataItems)

//console.log(cart)
}else{
  //console.log('корзина была пустая')
  product.isInCart = true
  cart.push(product)

  localStorage.setItem('cart',JSON.stringify(cart))
}

setCounter(cart.length)
setIsModalOpen(!isModalOpen)
setCurrentProductId(null)
setCardModal(!cardModal)

//console.log(counter)
 }

const deleteFromCart=(id,name)=>{
  
  let cart = JSON.parse(localStorage.getItem('cart'));
  let favorite =JSON.parse(localStorage.getItem('favorite'))
  const product = products.find(item=>item.article==id)
  //console.log(product)
  const elems= cart.find((cartItem)=> cartItem.article==product.article)
 
  localStorage.setItem('cart',JSON.stringify([...cart.slice(0,cart.indexOf(elems)),...cart.slice(cart.indexOf(elems)+1)]))
  
  const newCart = JSON.parse(localStorage.getItem('cart'))
  let updataItems= checkProducts(products,newCart,favorite)
  
  setProduct(updataItems)
  setCounter(newCart.length)
  setCurrentProductId(id)

}


  const addProductToFavorites=(id)=>{
  let favorite = JSON.parse(localStorage.getItem('favorite'));
  if(id){
  const product = products.find(item=> item.article==id)
  product.isInFavorite=!product.isInFavorite

  const elems= favorite.find((favoriteItem)=> favoriteItem.article==product.article)
  
  !elems && (favorite.push(product) && localStorage.setItem('favorite',JSON.stringify(favorite)))
   elems && localStorage.setItem('favorite',JSON.stringify([...favorite.slice(0,favorite.indexOf(elems)),...favorite.slice(favorite.indexOf(elems)+1)]))


  setCurrentProductId(id)
  setCurrentProduct(!curentProduct)
  setIsAddedToFavorite(!isAddedToFavorite)
   
}
 }

const handlePageChange =(pageName)=>{
setPage(pageName)
}

const handleCartItemModal = (article) => {
  setCurrentProductId(article)
  setDeleteCartModal(!deleteCartModal)
} 

console.log(page)

  return(<>
    <Header onLinkClick={handlePageChange}
      page={page}
    />
    <CartIcon counter={counter} />
    <Routes>

    <Route path="/home" element={
        <HomePage
      products={products}
      handleModalOpen={handleModal}
      addProductToFavorites={addProductToFavorites}
      isAddedToFavorite={isAddedToFavorite}
      page={page}
    />
    }></Route>


    <Route path="/home" element={
        <HomePage
      products={products}
      handleModalOpen={handleModal}
      addProductToFavorites={addProductToFavorites}
      isAddedToFavorite={isAddedToFavorite}
      page={page}
    />
    }>

    </Route>
    <Route path="/favorites" element={
       <FavPage
      products={products}
      handleModalOpen={handleModal}
      addProductToFavorites={addProductToFavorites}
      isAddedToFavorite={isAddedToFavorite}
      page={page}
      deleteFromCart={deleteFromCart}
      handleCartItemModal={handleCartItemModal}
      deleteCartModal={deleteCartModal} 
    /> 
    }>

    </Route>

    <Route path="/inCart" element={
        <CartPage
      products={products}
      handleModalOpen={handleModal}
      addProductToFavorites={addProductToFavorites}
      isAddedToFavorite={isAddedToFavorite}
      page={page}
      deleteFromCart={deleteFromCart}
      handleCartItemModal={handleCartItemModal}
      deleteCartModal={deleteCartModal}
      
    />
    }>

    </Route>
    
</Routes>

    
      {isModalOpen &&  <Modal
          onClose={handleModal}
          isModalOpen={isModalOpen}
          onSubmit={addProductToCart}
          id={curentProductId}
          name={`add to cart ${cardName}`}
          className={'modal-wrapper'}
          submitText={'Yes Please'}
          closeText={'No, thanks'}
      /> }

      {deleteCartModal && <Modal
             text={`do you want to delete from Cart`}
             closeText={'No i dont want to do it'}
             submitText={'Yes, please'}
             product={products}
             onClose={handleCartItemModal}
             deleteCartModal={deleteCartModal}
             deleteFromCart={deleteFromCart}
             id={curentProductId}
         /> }
    <CardModal cardModal={cardModal}
      name={cardName}
    />
    <Footer />
    </>)
}

export default Main


