import {ADD_PRODUCTS_TO_CART, REMOVE_PRODUCTS_FROM_CART,UPDATE_FROM_LS,MAKE_PURCHASE} from "../../types/types"

const store = {productsInCart:[],
  userData:[]
}

export default (state=store, action)=>{
 
  switch(action.type){
  case ADD_PRODUCTS_TO_CART:
   const product= state.productsInCart.find((productInCart)=> productInCart.article ==action.payload.article)
  const cart = JSON.parse(localStorage.getItem('cart'));

  const productInCart= cart.find((productInCart)=> productInCart.article ==action.payload.article)
    //console.log(cart)
   // console.log(action.payload)
  if(!product && !productInCart){
    let newCart= [...cart,action.payload]
  console.log(newCart)
    localStorage.setItem('cart',JSON.stringify(newCart))
    return {
      ...state,
      productsInCart:[...state.productsInCart, action.payload]
    }

  }else{
    return {...state}
  }
  
  case REMOVE_PRODUCTS_FROM_CART:
  const product_= state.productsInCart.find((productInCart)=> productInCart.article ==action.payload.article);
 
  const cart_ = JSON.parse(localStorage.getItem('cart'));
 
  const productCart_= cart_.find((cartItem)=> cartItem.article ==action.payload.article)
  const newCart = [...cart_.slice(0,cart_.indexOf(productCart_)),...cart_.slice(cart_.indexOf(productCart_)+ 1)]
 
  localStorage.setItem('cart',JSON.stringify(newCart))
  return {productsInCart:[...state.productsInCart.slice(0,state.productsInCart.indexOf(product_)),...state.productsInCart.slice(state.productsInCart.indexOf(product_)+ 1)]}
  
  case UPDATE_FROM_LS:

  return {...state, productsInCart:action.payload.cart}

  case MAKE_PURCHASE:
  
  localStorage.setItem('cart',JSON.stringify([]))
  return {...state,userData:action.payload,productsInCart:[]}
  

  default: 
  return state
 }
}