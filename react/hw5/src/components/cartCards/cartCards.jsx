import React, { useState,useEffect } from 'react';
import { useDispatch} from 'react-redux';
import { Icon } from '@iconify/react';
import '../productCard/productCard.css';
import Button from '../button/button';
import IconStar from '../productCard/svg';
import Modal from '../Modal/Modal';
import { removeFromCart,addToFavorite } from '../../redux/actions/posts';


const CartCards = ({item,cardInfavorite}) => {
  
//console.log(cardInfavorite)
  const [iconFilled, setIconFilled] = useState(false)
  const [isShownModal,setIsShownModal]=useState({
    favoriteModal:false,
    addToCartModal:false,
    deleteFormFavorite:false,
    deleteFromCart:false
  })
  useEffect(()=>{
    if(localStorage.hasOwnProperty('favorite')){
      if(localStorage.getItem('favorite').includes(item.name)){
        setIconFilled(!iconFilled)
      }
    }
     },[])

  const dispatch=useDispatch()
  return (
    <div className='card'>
     <IconStar fill = {iconFilled} /> 
     <div className="delete-btn">                                       
         <Icon icon="emojione-monotone:cross-mark-button"  width="30" height="30" onClick={()=>setIsShownModal({deleteFromCart:true})} 
         
          />

     </div>
     <div className='image-wrap'>
     <img className="fill" src={item.url} />
     </div>
     <div className='name'>Books name:{item.name}</div>
     <div className='price'>Price: {item.price}</div>
     <div className='color'>Color:{item.color}</div>
     <div className="card-action">
     
<Button text={'bay now'}

/>
{isShownModal.favoriteModal && <Modal name={item.name}
  onClick={()=>setIsShownModal({favoriteModal:false})}
  addItem={()=>{dispatch(addToFavorite(item))
    setIsShownModal({favoriteModal:false})
    setIconFilled(true)
    }
  }
text={'Do you want to add to Favorite'}
/>}

{isShownModal.deleteFromCart && <Modal name={item.name}
  onClick={()=>setIsShownModal({favoriteModal:false})}
  addItem={()=>{dispatch(removeFromCart(item))
    setIsShownModal({favoriteModal:false})}
  }
text={'Do you want to remove from Favorite'}
/>}
<Button text={'add to favorite'}
  onClick={()=>setIsShownModal({favoriteModal:true})}
/>

      </div>
      
     </div>

  );
};

export default CartCards;