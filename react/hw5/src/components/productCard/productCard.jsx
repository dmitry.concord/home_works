import React, { useEffect, useState } from 'react';
import { useDispatch} from 'react-redux';
import { Icon } from '@iconify/react';
import './productCard.css';
import Button from '../button/button';
import Modal from '../Modal/Modal';
import IconStar from './svg';
import { addToFavorite, addToCart } from '../../redux/actions/posts';


const ProductCard=({item})=>{
  const [isShownModal,setIsShownModal]=useState({
    favoriteModal:false,
    addToCartModal:false,
    deleteFormFavorite:false,
    deleteFromCart:false
  })

  const [iconFilled, setIconFilled] = useState(false)


  const dispatch = useDispatch()
 useEffect(()=>{
if(localStorage.hasOwnProperty('favorite')){
  if(localStorage.getItem('favorite').includes(item.name)){
    setIconFilled(!iconFilled)
  }
}
 },[])

  return (<div className='card'>
      <IconStar fill = {iconFilled} /> 
     <div className="delete-btn">                                       

     </div>
     <div className='image-wrap'>
     <img className="fill" src={item.url} />
     </div>
     <div className='name'>Books name:{item.name}</div>
     <div className='price'>Price: {item.price}</div>
     <div className='color'>Color:{item.color}</div>
     <div className="card-action">
     
<Button text={'bay now'}
onClick={()=>setIsShownModal({addToCartModal:true})}
/>
{isShownModal.favoriteModal && <Modal name={item.name}
  onClick={()=>setIsShownModal({favoriteModal:false})}
  addItem={()=>{dispatch(addToFavorite(item))
    setIsShownModal({favoriteModal:false})
    setIconFilled(true)}
   
  }
text={'Do you want to add to Favorite'}
/>}

{isShownModal.addToCartModal && <Modal name={item.name}
  onClick={()=>setIsShownModal({favoriteModal:false})}
  addItem={()=>{dispatch(addToCart(item))
    setIsShownModal({favoriteModal:false})}
  }
text={'Do you want to add to Cart'}
/>}

<Button text={'add to favorite'}
  onClick={()=>setIsShownModal({favoriteModal:true})}
/>
      </div>
      
     </div>

  );
};

export default ProductCard;