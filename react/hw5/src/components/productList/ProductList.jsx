import React from 'react';
import { useSelector } from 'react-redux';
import posts from '../../redux/reducer/posts';
import ProductCard from '../productCard/productCard'
import './productList.css'

const ProductList = () => {

  const posts = useSelector((state)=> state.posts)
  //console.log(posts.data)
  return (

   !posts.data ? 'NO ITEMS': <div className="items">{(posts.data.map((item)=>{
    
   return <div key={item.article} > <ProductCard 
    item={item}
   />

   </div> 
   }))}</div>
  );
};

export default ProductList;