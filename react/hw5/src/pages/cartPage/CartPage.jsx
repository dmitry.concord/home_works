import React, {useEffect} from 'react';
import { useDispatch,useSelector } from 'react-redux';
import CartCards from '../../components/cartCards/cartCards';
import '../Home/home.css';
import CartIcon from '../../components/cartIcon/CartIcon';
import Form from '../../components/form/Form.formik';
import { updateLocalStorage } from '../../redux/actions/posts';

const CartPage = () => {
const dispatch= useDispatch()
    useEffect(()=>{
    dispatch(updateLocalStorage())
  },[])

  
  const productsInCart = useSelector(({productsInCart})=>productsInCart.productsInCart)
 
  return (<><CartIcon cartLenght={productsInCart.length}
  />
  
    <div className='items-wrapper'>
      CART
      <div className="items"> {productsInCart.map((item)=>(<div key={item.article}>
      <CartCards item={item} />

     </div>))} 
      <Form/> 
     </div>
    </div>
    
    </> );
};

export default CartPage;