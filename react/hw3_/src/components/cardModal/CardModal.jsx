import { Component } from 'react';
import "./cardModal.css";
import Button from '../button/Button';


function CardModal({text,product,addItem}){

 return(
   <>
    <div className={'modal-wrapper'}>
        <div className="modal-header">
            <h2 className="modal-title">
                <p className="modal-text">{text}{product.name}? </p>
                <Button type="button" className={"close"} text={'X'}/>

            </h2>
        </div>
        <div className="modal-body">

        </div>
        <div className="button-container">

          <div className='button-container'>
                <Button text={'ok'}
                        className={'btn'}
                        onClick={()=>addItem(product)}    
                />
                <Button 
                        text={'deny'}
                        className={'btn'}
                       
                />
            </div> 
            

        </div>
    </div>
</>
 )
//}
}

export default CardModal