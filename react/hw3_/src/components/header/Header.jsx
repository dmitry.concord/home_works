import './header.css';
import logo from './logo.png'

import {Link} from 'react-router-dom'


import React from 'react';

const Header = () => {
    return (
        <> <header className='container'>
        <div className='header-wrap'>
            <div className='logo'>
                 <div className='logo-pic'><img src={logo} alt="logo" />
                </div> 
                <p className="logo-text">BOOKSHOP</p>
            </div>
            <nav className='header-menu'>
                <ul className="header-menu-list">
                 <li className="menu-item"><Link className='link' to="/" >HOME</Link></li>
                 <li className="menu-item"><Link className='link' to="/favorite" >FAVORITES</Link></li>
                 <li className="menu-item"><Link className='link' to="/incart" >IN Cart</Link></li>
                 <li className="menu-item"><a href="#">NEWS</a></li>
                </ul>
            </nav>
            <div className="nav-search">  
            </div>
        </div>
        
        <section className="section-news">
            <div className="news-title">
                <p>THE LATEST NEWS ON DESIGN & ARCHITECTURE</p>
            </div>
        
            <div className='btn-wrap'>
                <button className='button left' type='button'>Subscribe Now</button>
        
                <button className='button right' type='button'>Best Articles</button>
        
            </div>
        </section>
        </header></>
    );
};

export default Header;
