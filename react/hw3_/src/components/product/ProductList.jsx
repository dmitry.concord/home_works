import React from 'react';
import ProductCard from "./ProductCard";



const ProductList = ({products,cartStorage,favoriteStorage,setFavoriteStorage,setIsLoading,isLoading}) => {
    return (
      !products.data ? 'NO ITEMS': <div className="items">{(products.data.map((product)=>{
    
        return <div key={product.article} > <ProductCard 
         product={product}
         cartStorage={cartStorage}
         favoriteStorage={favoriteStorage}
         setFavoriteStorage={setFavoriteStorage}
         setIsLoading={setIsLoading}
         isLoading={isLoading}
        />
     
        </div> 
        }))}</div>
       
    );
};

export default ProductList;

