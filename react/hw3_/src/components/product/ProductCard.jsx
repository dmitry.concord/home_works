import React, { useState, useEffect } from 'react';
import Button  from '../button/Button';
import { Icon } from '@iconify/react';
import IconStar from './svg';
import './productCard.css';
import Modal from '../cardModal/CardModal'

const ProductCard = ({product,cartStorage,favoriteStorage,setIsLoading,isLoading}) => {

  const[isOpenModal, setIsOpenModal]= useState({
    favoriteModal:false,
    addToCartModal:false,
    deleteFormFavorite:false,
    deleteFromCart:false
  })
  const [iconFilled, setIconFilled] = useState(false)

  useEffect(()=>{
    if(localStorage.hasOwnProperty('favorite')){
      if(localStorage.getItem('favorite').includes(product.name)){
        setIconFilled(!iconFilled)
      }
    }
     },[])

  const addToCart =(product)=>{
    const productIncart = cartStorage.find(item=> item.article==product.article)

    if(!productIncart){
      cartStorage.push(product)
    }
  localStorage.setItem('cart',JSON.stringify(cartStorage))
  setIsLoading(isLoading+1)
  setIsOpenModal({addToCartModal:false})
  }

  const addToFavorite=(product)=>{
    const productInFavorite = favoriteStorage.find((item)=> item.article==product.article)
    if(!productInFavorite){
      favoriteStorage.push(product)
    }
    localStorage.setItem('favorite',JSON.stringify(favoriteStorage))
    setIsOpenModal({favoriteModal:false})
  
  }

  return (
    <div className='card'>
       <IconStar iconFilled={iconFilled} /> 
     <div className="delete-btn">
         

     </div>
     <div className='image-wrap'>
      <img className="fill" src={product.url} /> 
     </div>
     <div className='name'>Books name:{product.name}</div>
     <div className='price'>Price: {product.price}</div>
     <div className='color'>Color:{product.color}</div>
     <div className="card-action">
     

      <Button text={'bay now'}
             className={'btn-small'}
             onClick={()=>setIsOpenModal({addToCartModal:true})
             }
     />
         
          {isOpenModal.addToCartModal && <Modal text={'add to Cart'}
            product={product}
            addItem={addToCart}
             />}
             {isOpenModal.addToFavorite && <Modal text={'add to favorite'}
              addItem={()=>{
                setIconFilled(true)
                addToFavorite(product)}}
              product={product}
             />}
     <Button 
            className={'btn'}   
            text={'add to Favorite'}
            onClick={()=>setIsOpenModal({addToFavorite:true})}
             />
          
      </div>
      
     </div>
  );
};

export default ProductCard;





