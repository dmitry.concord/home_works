
import ProductCard from "../product/ProductCard"

function Button({text,className,onClick,data_modal_id}){
  //console.log(data_modal_id)
  return(<>
    <button data-id={data_modal_id} className={className} onClick={onClick}>{text}</button>
    </>)
}

export default Button