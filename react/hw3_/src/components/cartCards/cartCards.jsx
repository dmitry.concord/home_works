import React, { useState, useEffect } from 'react';
import { Icon } from '@iconify/react';
import IconStar from '../product/svg';
import Button from '../button/Button';
import CardModal from '../cardModal/CardModal';



const CartCards = ({item,setCartStorage,cartStorage,favoriteStorage}) => {
  
  const [isShownModal,setIsShownModal]=useState({
    favoriteModal:false,
    addToCartModal:false,
    deleteFormFavorite:false,
    deleteFromCart:false
  })
  const [iconFilled, setIconFilled] = useState(false)
  useEffect(()=>{
    if(localStorage.hasOwnProperty('favorite')){
      if(localStorage.getItem('favorite').includes(item.name)){
        setIconFilled(!iconFilled)
      }
    }
     },[])
  const addToFavorite=(product)=>{
    const productInFavorite = favoriteStorage.find((item)=> item.article==product.article)
    if(!productInFavorite){
      favoriteStorage.push(product)
    }
    localStorage.setItem('favorite',JSON.stringify(favoriteStorage))
    setIconFilled(!iconFilled)
    setIsShownModal({favoriteModal:false})
  }


  const deleteFromCart=(product)=>{
    const productIncart = cartStorage.find((item)=> item.article==product.article)
    if(productIncart){
      cartStorage = [...cartStorage.slice(0,cartStorage.indexOf(productIncart)), ...cartStorage.slice(cartStorage.indexOf(productIncart)+1)]
      setCartStorage(cartStorage)
    }
    localStorage.setItem('cart',JSON.stringify(cartStorage))
    setIsShownModal({deleteFromCart:false})
  }
  
  return (
    <div className='card'>
      <IconStar iconFilled={iconFilled} /> 
     <div className="delete-btn">                                       
         <Icon icon="emojione-monotone:cross-mark-button"  width="30" height="30" onClick={()=>setIsShownModal({deleteFromCart:true})}  />
     </div>
     <div className='image-wrap'>
     <img className="fill" src={item.url} />
     </div>
     <div className='name'>Books name:{item.name}</div>
     <div className='price'>Price: {item.price}</div>
     <div className='color'>Color:{item.color}</div>
     <div className="card-action">
     
<Button text={'bay now'}
/>
{isShownModal.favoriteModal && <CardModal product={item}
addItem={addToFavorite}
text={'Do you want to add to Favorite'}
/>}

{isShownModal.deleteFromCart && <CardModal product={item}
addItem={deleteFromCart}
text={'Do you want to remove from Favorite'}
/>}
<Button text={'add to favorite'}
  onClick={()=>setIsShownModal({favoriteModal:true})}
/>

      </div>
      
     </div>

  );
};

export default CartCards;