import { Icon } from '@iconify/react';
import "./cartIcon.css"


function CartIcon({cartLenght}){

//console.log(cartLenght)
  return(<div className='cart-icon' >
  <Icon icon="emojione:shopping-cart" width="40" height="40" />  
  <span className='quantity'>{cartLenght}</span> 

  </div>)
}

export default CartIcon