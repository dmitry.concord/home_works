import React, { useState } from 'react';
import { Icon } from '@iconify/react';
import IconStar from '../product/svg';
import Button from '../button/Button';
import CardModal from '../cardModal/CardModal';




const FavCards = ({item,cartStorage,favoriteStorage,setFavoriteStorage,isLoading,setIsLoading}) => {
  
  const [isShownModal,setIsShownModal]=useState({
    favoriteModal:false,
    addToCartModal:false,
    deleteFormFavorite:false,
    deleteFromCart:false
  })

   const addToCart =(product)=>{
    const productIncart = cartStorage.find(item=> item.article==product.article)
    if(!productIncart){
      cartStorage.push(product)
    }
    localStorage.setItem('cart',JSON.stringify(cartStorage))
    setIsShownModal({addToCartModal:false})
    setIsLoading(isLoading+1)
  } 

  const deleteFromFavorite =(product)=>{
    const productInFavorite = favoriteStorage.find(item=> item.article==product.article)
  
    if(productInFavorite){
    
    favoriteStorage = [...favoriteStorage.slice(0,favoriteStorage.indexOf(productInFavorite)), ...favoriteStorage.slice(favoriteStorage.indexOf(productInFavorite)+ 1)]
       setFavoriteStorage(favoriteStorage) 
    }
    localStorage.setItem('favorite',JSON.stringify(favoriteStorage))
    console.log(favoriteStorage)
    setIsShownModal({addToCartModal:false})
  }
  
  return (
    <div className='card'>
      <IconStar iconFilled={true} />  
     <div className="delete-btn">                                       
         <Icon icon="emojione-monotone:cross-mark-button"  width="30" height="30" onClick={()=>setIsShownModal({deleteFromCart:true})} 
         
          />

     </div>
     <div className='image-wrap'>
     <img className="fill" src={item.url} />
     </div>
     <div className='name'>Books name:{item.name}</div>
     <div className='price'>Price: {item.price}</div>
     <div className='color'>Color:{item.color}</div>
     <div className="card-action">
     
<Button text={'bay now'}
onClick={()=>setIsShownModal({addToCartModal:true})}
/>
{isShownModal.addToCartModal && <CardModal product={item}
addItem={addToCart}
text={'Do you want to add to Cart '}
/>}

{isShownModal.deleteFromCart && <CardModal product={item}
 addItem={deleteFromFavorite}
text={'Do you want to remove from Favorite'}
/>}
<Button text={'add to favorite'}


/>

      </div>
      
     </div>

  );
};

export default FavCards;