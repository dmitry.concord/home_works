import React from 'react';
import {Route,Routes} from 'react-router-dom'
import Header from '../components/header/Header';
import HomePage from '../pages/HomePage';
import CartPage from '../pages/CartPage';
import FavPage from '../pages/FavPage';



const AppRouts = ({products,cartStorage,favoriteStorage,setFavoriteStorage,setCartStorage,setIsLoading,isLoading}) => {
//console.log(setFavoriteStorage)
  //console.log(cartStorage)
  return (
    <>
    
  <Header/>
  <Routes>
    <Route exact path='/' element={(
      <HomePage products={products}
      cartStorage={cartStorage}
      favoriteStorage={favoriteStorage}
      setFavoriteStorage={setFavoriteStorage}
      setCartStorage={setCartStorage}
      setIsLoading={setIsLoading}
      isLoading={isLoading}
      />
    )
    }


    />

<Route path='/incart' element={(

  <CartPage cartStorage={cartStorage}
  setCartStorage={setCartStorage}
  favoriteStorage={favoriteStorage}
  //setIsLoading={setIsLoading}
 // isLoading={isLoading}
  />
)}
/>
<Route path='/favorite' element={(
<FavPage favoriteStorage={favoriteStorage}
  cartStorage={cartStorage}
  setFavoriteStorage={setFavoriteStorage}
  setIsLoading={setIsLoading}
  isLoading={isLoading}
/>
)}

/>
   
  </Routes>
  </>
  );
};

export default AppRouts;