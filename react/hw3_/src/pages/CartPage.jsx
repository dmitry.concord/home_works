import React from 'react';
import CartCards from '../components/cartCards/cartCards';
import CartIcon from '../components/cartIcon/CartIcon';

const CartPage = ({cartStorage,setCartStorage,favoriteStorage}) => {


  return (<><CartIcon cartLenght={cartStorage.length}/><div className='items-wrapper'>
  InCart
  <div className="items"> { !cartStorage ? "no item" :(cartStorage.map((product)=>(<div key={product.article}>
<CartCards  item={product} 
cartStorage={cartStorage} 
setCartStorage={setCartStorage}
favoriteStorage={favoriteStorage}
/>

</div> )))}  
 </div>
</div>
</>
    
  );
  

};

export default CartPage;