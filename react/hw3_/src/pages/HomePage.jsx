
import React, {useEffect} from 'react';
import ProductList from '../components/product/ProductList';
import CartIcon from '../components/cartIcon/CartIcon';

const HomePage = ({products,cartStorage,favoriteStorage,setFavoriteStorage,setCartStorage,setIsLoading,isLoading}) => {
 
  return (<> <CartIcon cartLenght={cartStorage.length}/>
  <div className='items-wrapper'>
  <ProductList products={products}
    cartStorage={cartStorage}
    favoriteStorage={favoriteStorage}
    setFavoriteStorage={setFavoriteStorage}
    setIsLoading={setIsLoading}
    isLoading={isLoading}
  />
  </div></>
   
  );
};

export default HomePage;

