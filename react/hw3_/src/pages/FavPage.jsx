import React,{useEffect} from 'react';
import FavCards from '../components/favCards/FavCards';
import CartIcon from '../components/cartIcon/CartIcon';

const FavPage = ({products,favoriteStorage,cartStorage,setFavoriteStorage,setIsLoading,isLoading}) => {

  return (<><CartIcon cartLenght={cartStorage.length}/><div className='items-wrapper'>
  FaVORITES
  <div className="items"> { !favoriteStorage ? "no item" :(favoriteStorage.map((product)=>(<div key={product.article}>
<FavCards  item={product} 
favoriteStorage={favoriteStorage}
cartStorage={cartStorage}
setFavoriteStorage={setFavoriteStorage}
setIsLoading={setIsLoading}
isLoading={isLoading}
 />
</div> )))}  
 </div>
</div>
</>
    
  );
};

export default FavPage;
