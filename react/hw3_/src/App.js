import logo from './logo.svg';
import './App.css';
import AppRouts from './route/AppRouts';
import { useEffect, useState } from 'react';
import axios from 'axios';

function App() {

  const[product, setProduct]= useState([])
  const[cartStorage, setCartStorage] = useState([])
  const[favoriteStorage,setFavoriteStorage] = useState([])
  const[isLoading,setIsLoading]= useState(0)
  
  const fetch = async ()=>{
  const {data} = await axios.get('http://localhost:3000/product.json')
  setProduct(data)
  return data
}
  useEffect(()=>{
   fetch()

   if(localStorage.getItem('cart')){
    const data =JSON.parse(localStorage.getItem('cart'))
    setCartStorage(data)
   }

   if(localStorage.getItem('favorite')){
    const data = JSON.parse(localStorage.getItem('favorite'))
    setFavoriteStorage(data)
   }
  
  
  },[])

  useEffect(()=>{
    if(localStorage.getItem('cart')){
      const data =JSON.parse(localStorage.getItem('cart'))
      setCartStorage(data)
     }
  
     if(localStorage.getItem('favorite')){
      const data = JSON.parse(localStorage.getItem('favorite'))
      setFavoriteStorage(data)
     }
  },[isLoading])


  return (
    <>
<AppRouts products={product}
  cartStorage={cartStorage}
  setCartStorage={setCartStorage}
  favoriteStorage={favoriteStorage}
  setFavoriteStorage={setFavoriteStorage}
  setIsLoading={setIsLoading}
  isLoading={isLoading}
/>
    </>
  );
}

export default App;


