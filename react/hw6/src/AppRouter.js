import React, {useContext,useEffect,useState} from 'react';
import { Route,Routes } from 'react-router-dom';
import { Provider } from 'react-redux';
import Header from './components/header/Header';
import Favorites from './pages/favorites/Favorites';
import Home from './pages/Home/Home';
import CartPage from './pages/cartPage/CartPage';
import { CardsContext } from './components/cardsContext/CardsContext';

 


const AppRouter = () => {
  const [regime,setRegime] = useState('')
  const toggleRegime =(regime)=> setRegime(regime);
   
  
  //console.log(regime)
  
 
  return (<>
  <CardsContext.Provider value={{toggleRegime,regime}}> 
  <Header />
  <Routes>
  <Route exact path='/' element={
      <Home/>
    
    }
    />
    <Route path='/favorites' element={

      <Favorites/>
    }/>

    <Route path='/incart' element={
   <CartPage/> }/>
    
  </Routes>

  </CardsContext.Provider> 
  </>
 
  );
};

export default AppRouter;