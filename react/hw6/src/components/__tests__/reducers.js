import favorite from "../../redux/reducer/favorite";
import {ADD_PRODUCTS_IN_FAVORITES} from '../../types/types';
import {addToFavorite} from '../../redux/actions/posts'
//import { useDispatch, } from "react-redux";

describe('favorites reducer', ()=>{
  //const dispatch = useDispatch()
  const store = [{"name":"Вино из одуванчиков","article":"e2204"},
    {"name":"Суворі чоловіки","article":"e2205"}]

console.log(store)
    /* jest.mock('react-redux', () => {
      const ActualReactRedux = jest.requireActual('react-redux');
      return {
          ...ActualReactRedux,
          useSelector: jest.fn().mockImplementation(() => {
              return mockState;
          }),
      };
  });
    expect((undefined,[])).toEqual([])
  }) */
  it('should return default value', ()=>{
    
    expect((undefined,[])).toEqual([])
  })
  
   it('should add to favorite', ()=>{

//data={"name":"Вино из одуванчиков","article":"e2208"}
console.log(store)
     expect((store, {
      type:ADD_PRODUCTS_IN_FAVORITES, payload: {"name":"1655","article":"e2208"}

    })).toEqual([{"name":"Вино из одуванчиков","article":"e2204"},
    {"name":"Суворі чоловіки","article":"e2205"},{"name":"1655","article":"e2208"}])
    
  }) 
})  