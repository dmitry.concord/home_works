import React, {useEffect, useRef} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';
import './form.css';
import * as Yup from 'yup';
import Button from '../button/button';
//import { isDisabled } from '@testing-library/user-event/dist/utils';
import { makePurchase } from '../../redux/actions/posts';


const initialValues={name:'',
surname:'',
age:'',
adress:'',
phone:''
}


const Form = () => {

  const dispatch= useDispatch()
  const validate = values =>{
    let errors ={}
    if(!values.name){
     errors.name='Required'
    }else if(values.name[0].toUpperCase() + values.name.slice(1) !== values.name){
     errors.name='The first letter of the name must be capitalized'
    }
    if(!values.surname){
     errors.surname='Required'
    }else if(values.surname[0].toUpperCase() + values.surname.slice(1) !== values.surname){
      errors.surname='The first letter of the name must be capitalized'
     }
     return errors
  }
  
  
  const onSubmit= values=>{

  dispatch(makePurchase(values))
    }
  const productsInCart = useSelector(({productsInCart})=>{ if(productsInCart.userData && productsInCart.productsInCart.length!=0)console.log('SUCCESS:',productsInCart)})
  const validationSchema = Yup.object({
      name:Yup.string().max(10),
      surname:Yup.string().required('Required'),
      age:Yup.number().min(18)
      
      .required('Required'),
      adress:Yup.string().required('Required'),
      phone:Yup.string().min(5)
      .max(12)
      
      .required('Required')
    })


const formik = useFormik({initialValues,onSubmit,validate,validationSchema})

const errors = Object.values(formik.errors);
  return (
  
      <div className="form-wrapper">
            <div className="header-modal">
              
              <a className="link" href="#">Log in</a>
            </div>

            <div className="form-buttons">
              <button className="button blue iconed">
                <span className="iconify" data-icon="bx:bxl-facebook"></span>
                <span className="text">Facebook</span>
              </button>
              <button className="button red iconed">
                <span
                  className="iconify"
                  data-icon="ant-design:google-plus-outlined"
                ></span>
                <span className="text">Google</span>
              </button>
            </div>
            <p><span>or</span></p>

            <form className="login-form" action="" onSubmit={formik.handleSubmit}>
              <input type="text" name="name" placeholder="FirstName" id="name" {...formik.getFieldProps('name')} />
              {formik.touched.name && formik.errors.name ? (<div className='error'>{formik.errors.name}</div>): null}
              <input type="text" name="surname" placeholder="LastName" id="surname" {...formik.getFieldProps('surname')}/> 
              {formik.touched.surname && formik.errors.surname ? (<div className='error'>{formik.errors.surname}</div>): null}
              <input type="number" name="age" placeholder="AGE" id="age" {...formik.getFieldProps('age')} />
              {formik.touched.age && formik.errors.age ? (<div className='error'>{formik.errors.age}</div>): null}
              <input type="text" name="adress" placeholder="Indicate Your Adress" id="adress" {...formik.getFieldProps('adress')} />
              {formik.touched.adress && formik.errors.adress ? (<div className='error'>{formik.errors.adress}</div>): null}
              <input type="tel" name="phone" placeholder="Contact number" id="phone" {...formik.getFieldProps('phone')}/> 
              {formik.touched.phone && formik.errors.phone ? (<div className='error'>{formik.errors.phone}</div>): null}
              <label>
              <input type="checkbox" value="" /> <span className="checkbox-text">Agree with our</span>  
                <a className="input-link" href="">Terms & Conditions</a>
              </label>

               <button className="button green" type="submit" disabled={errors && !formik.dirty}>
                 <span className="text">CHECKOUT</span>
              </button> 
             
            </form>
          </div>
   
  );
};

export default Form;