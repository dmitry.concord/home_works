import React, { useContext } from 'react';
import { useSelector } from 'react-redux';
import posts from '../../redux/reducer/posts';
import ProductCard from '../productCard/productCard'
import './productList.css'

import { CardsContext } from '../cardsContext/CardsContext';

  const ProductList = () => {

  const posts = useSelector((state)=> state.posts)
  
  const {regime} = useContext(CardsContext)
  //console.log(regime)
  return (

   !posts.data ? 'NO ITEMS': <div className={regime}>{(posts.data.map((item)=>{
    
   return <div key={item.article} className="cards-item" > <ProductCard 
    item={item}
   />

   </div> 
   }))}</div>
  );
};

export default ProductList;