import React, { useState } from 'react';
import { useDispatch} from 'react-redux';
import { Icon } from '@iconify/react';
import '../productCard/productCard.css';
import Button from '../button/button';
import Modal from '../Modal/Modal';
import IconStar from '../productCard/svg';
import CartIcon from '../cartIcon/CartIcon';
import { addToFavorite,removeFromFavorite,addToCart } from '../../redux/actions/posts';

const FavoriteCard = ({item,cardInFavorite}) => {
  const [isShownModal,setIsShownModal]=useState({
    favoriteModal:false,
    addToCartModal:false,
    deleteFormFavorite:false,
    deleteFromCart:false
  })
  const dispatch=useDispatch()
  return (
    <div className='card'>
    <IconStar cardInFavorite={cardInFavorite}/> 
     <div className="delete-btn">                                       
         <Icon icon="emojione-monotone:cross-mark-button"  width="30" height="30" onClick={()=>setIsShownModal({deleteFormFavorite:true})}/>

     </div>
     <div className='image-wrap'>
     <img className="fill" src={item.url} />
     </div>
     <div className='name'>Books name:{item.name}</div>
     <div className='price'>Price: {item.price}</div>
     <div className='color'>Color:{item.color}</div>
     <div className="card-action">
     
<Button text={'bay now'}
onClick={()=>setIsShownModal({addToCartModal:true})}
/>
<Button text={'add to favorite'}

/>
{isShownModal.addToCartModal && <Modal name={item.name}
  onClick={()=>setIsShownModal({favoriteModal:false})}
  addItem={()=>{dispatch(addToCart(item))
    setIsShownModal({favoriteModal:false})}
  }
text={'Do you want to add to Cart'}
/>}

{isShownModal.deleteFormFavorite && <Modal name={item.name}
  onClick={()=>setIsShownModal({favoriteModal:false})}
  addItem={()=>{dispatch(removeFromFavorite(item))
    setIsShownModal({favoriteModal:false})}
  }
text={'Do you want to remove from Favorite'}
/>}

      </div>
      
     </div>

  );
};

export default FavoriteCard;