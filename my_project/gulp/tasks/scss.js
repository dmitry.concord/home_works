module.exports = function(){
  $.gulp.task('scss', function(){
    return $.gulp.src('src/static/scss/main.scss')
    .pipe($.gp.sourcemaps.init())
   .pipe($.sass({'include css':true
  })) 
    /* .pipe($.gp.scss({
      'include css':true
    })) */
    .pipe($.gp.autoprefixer({
      browsersList: ['last 10 versions'],
      cascade: false
    }))
    .on("error", $.gp.notify.onError({
      message: "Error: <%= error.message %>",
      title: "Error running something"
    }))
    .pipe($.gp.csso())
    .pipe($.gp.sourcemaps.write())
    .pipe($.gulp.dest('build/static/css/'))
    /* .on('end', browserSync.reload); */
    .pipe($.browserSync.reload({
      stream:true
    }))
  }); 
}