module.exports = function(){
  $.gulp.task('img', function(){
    return $.gulp.src('src/static/img/*.{png,jpg,gif}')
    .pipe($.gp.pug({
      pretty:true
    }))
    .pipe($.gp.tinypng('8368Bs7v4v4Plfwmbjcbfhpl32XGk5fn'))
    .pipe($.gulp.dest('build/static/img/'));
  });
 
}


