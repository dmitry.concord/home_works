import gulp from 'gulp'; 
import clean from 'gulp-clean';
import autoprefixer from 'gulp-autoprefixer';
import cleanCSS from 'gulp-clean-css';
import concat from 'gulp-concat';
/* import minify from 'gulp-js-minify'; */ 
import BS from 'browser-sync';
const browserSync = BS.create();
import gulpSass from 'gulp-sass';
import dartSass from 'sass';
import webp from 'gulp-webp';
import rename from 'gulp-rename';
import uglifyES from "gulp-uglify";
import cssnano from "cssnano";
import fileInclude from 'gulp-file-include';
const { task, parallel, watch, series, lastRun, src, dest } = gulp;
const sass = gulpSass(dartSass);

gulp.task('clean', () => gulp.src(['dist/css/', 'dist/js/'], {read: false}).pipe(clean())); 

gulp.task('buildCss', () => gulp.src('src/scss/**/*', {allowEmpty: true})
  .pipe(sass())
  .pipe(autoprefixer({cascade: false}))
  .pipe(cleanCSS({compatibility: 'ie8'}))
  .pipe(concat('styles.min.css'))
  .pipe(gulp.dest('dist/css')));


gulp.task('buildJs', () => gulp.src('src/js/**/*', {allowEmpty: true})
  .pipe(uglifyES('script.min.js'))
	.pipe(rename({ suffix: ".min" })) 
  .pipe(gulp.dest('dist/js')));


gulp.task('buildImg', () => gulp.src('src/img/**/*', {allowEmpty: true})
.pipe(
  webp({
      quality: 70
  })
)
  .pipe(gulp.dest('dist/img')));

gulp.task('build', gulp.series(/* 'clean', */ 'buildCss', 'buildJs', 'buildImg'));

gulp.task('sync', () => {
  browserSync.init({
    server: {
      baseDir: "./"
    }
  });
  gulp.watch(['src/**/*', 'index.html']).on('change', gulp.series('buildCss', 'buildJs', 'buildImg', browserSync.reload));
});

 gulp.task('dev', gulp.series('build','sync')); 