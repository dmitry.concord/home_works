let objImg = {
  "graphic-design"  : [
  "pic/graphic_design/graphic-design10.jpg",
  "pic/graphic_design/graphic-design11.jpg",
  "pic/graphic_design/graphic-design12.jpg",
  ],
  "landing-pages"  : [
          "pic/landing_page/landing-page5.jpg",
          "pic/landing_page/landing-page6.jpg",
          "pic/landing_page/landing-page7.jpg",
      ],
  
  "web-design" : [
  "pic/web_design/web-design5.jpg",
  "pic/web_design/web-design6.jpg",
  "pic/web_design/web-design7.jpg",
  ],
  
  "worldpress"  : [
    "pic/wordpress/wordpress8.jpg",
    "pic/wordpress/wordpress9.jpg",
    "pic/wordpress/wordpress10.jpg"
  ]
  } 

    let images = document.querySelector('.work-images');
    let getArrImg = function(someObj)  {
    /* let workImg = document.querySelectorAll('.work-img');  */
    let objVal =  Object.values(someObj);
    let objKey = Object.keys(someObj); 
       

    for(let k=0; k <objVal.length; k++){
    
    for(let i=0; i < objVal[k].length; i++){
      
    let divWrap = document.createElement('div');
    divWrap.classList.add('work-img',`${objKey[k]}`);
  
    let img = document.createElement('img');
    img.src = objVal[k][i];
    divWrap.appendChild(img);
    images.appendChild(divWrap);
    } 
 };
     return images.children;
} 
  
   let filter =  document.querySelector('.works-filter');
   let loadMoreNBtn = document.querySelector('.loadMore');
   let loader = document.querySelector('.loader');

function load(){
loader.style.display = 'block';
/* setTimeout(loadMoreNBtn.removeEventListener('click',load),3000); */
} 

  function imgLoader(event){
  getArrImg(objImg)
  loadMoreNBtn.style.display = 'none';
  loader.style.display = 'none';
    }/* 
   loadMoreNBtn.addEventListener('click', f1) */ 
   loadMoreNBtn.addEventListener('click', () => setTimeout(() => imgLoader(),3000) )
   loadMoreNBtn.addEventListener('click', load ) 
  
  // loadMoreNBtn.addEventListener('click', setTimeout(() => f1(),3000) )
 /* setTimeout(()=>{
      
  loadMoreNBtn.addEventListener('click', f1())
 },3000); */

function f2(event){ 
let workImg = document.querySelectorAll('.work-img');
let worksBtn= document.querySelectorAll('.works-button');

if(event.target.tagName !== 'BUTTON') {
  return false;
} else {
    
worksBtn.forEach(elem=>{
    elem.classList.remove('active')
  })
    event.target.classList.add('active')
  }
  let filterClass = event.target.dataset['filter'];
  console.log(workImg)
  workImg.forEach(elem =>{
    elem.classList.remove('hide');
    if(!elem.classList.contains(filterClass) && filterClass !=='all'){ 
      // loadMoreNBtn.style.display ='none'  
      loadMoreNBtn.style.display = 'none' ;
      console.log(filterClass)
      elem.classList.add('hide');
    }
    if (filterClass == 'all') {
      // const btn = document.querySelector('.loadMore') 
      if(loadMoreNBtn){
        loadMoreNBtn.style.display = 'block'
      }
      if(workImg.length > 12){
        console.log(workImg)
        const img = [...workImg]
        const newImg = img.slice(0,11)
        console.log(newImg)
  renderImg(newImg)
    } 
    } 
  }
  
  )}
function renderImg(img){
   const images = [...document.querySelector('.work-images').children];
  //  images.children.remove()
  images.forEach((item, index) => {
    if(index === 12){
      item.remove()
      loadMoreNBtn.style.display = 'block'
    }
  })
   console.log(images)
}
filter.addEventListener('click',f2 )
 let blockText;
 let blockDescrip;
  const createHoverBlock = () => {
  const hoverBlock = document.createElement("div");
  hoverBlock.classList.add("work-container-hover");
  let linkWrapper = document.createElement("div");
  linkWrapper.classList.add("work-container-hover-link-wrapper");
  let blokLink = document.createElement("a");
  let blockSearch = document.createElement("a");
  blokLink.innerHTML =  `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" width="25" height="25" preserveAspectRatio="xMidYMid meet" viewBox="0 0 36 36"><path d="M17.6 24.32l-2.46 2.44a4 4 0 0 1-5.62 0a3.92 3.92 0 0 1 0-5.55l4.69-4.65a4 4 0 0 1 5.62 0a3.86 3.86 0 0 1 1 1.71a2 2 0 0 0 .27-.27l1.29-1.28a5.89 5.89 0 0 0-1.15-1.62a6 6 0 0 0-8.44 0l-4.7 4.69a5.91 5.91 0 0 0 0 8.39a6 6 0 0 0 8.44 0l3.65-3.62h-.5a8 8 0 0 1-2.09-.24z" class="clr-i-outline clr-i-outline-path-1" /><path d="M28.61 7.82a6 6 0 0 0-8.44 0l-3.65 3.62h.49a8 8 0 0 1 2.1.28l2.46-2.44a4 4 0 0 1 5.62 0a3.92 3.92 0 0 1 0 5.55l-4.69 4.65a4 4 0 0 1-5.62 0a3.86 3.86 0 0 1-1-1.71a2 2 0 0 0-.28.23l-1.29 1.28a5.89 5.89 0 0 0 1.15 1.62a6 6 0 0 0 8.44 0l4.69-4.65a5.92 5.92 0 0 0 0-8.39z" class="clr-i-outline clr-i-outline-path-2" /></svg>` ;
  blockSearch.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" width="24" height="24" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path  d="M20.71 19.29l-3.4-3.39A7.92 7.92 0 0 0 19 11a8 8 0 1 0-8 8a7.92 7.92 0 0 0 4.9-1.69l3.39 3.4a1 1 0 0 0 1.42 0a1 1 0 0 0 0-1.42zM5 11a6 6 0 1 1 6 6a6 6 0 0 1-6-6z"/></svg>`;
  linkWrapper.appendChild(blokLink);
  linkWrapper.appendChild(blockSearch);
  blockText = document.createElement("p");
  blockText.textContent = "creative design";
  blockText.classList.add('hower-creative-text')
  blockDescrip = document.createElement("p");
  blockDescrip.textContent = "Web Design";
  hoverBlock.appendChild(linkWrapper);
  hoverBlock.appendChild(blockText);
  hoverBlock.appendChild(blockDescrip);
  return hoverBlock;
};
function renderHoverBlock(event){
  if(event.target.getAttribute('src')!= null){
   let contentBox = createHoverBlock();
   blockDescrip.innerHTML = event.target.parentElement.classList[1];
   console.log(contentBox)
   event.target.parentElement.insertAdjacentElement('afterbegin', contentBox);
   console.log(event.target.parentElement)
   //event.target.parentElement.before(contentBox);
  console.log(event.target.parentElement.classList[1])
 } 
  console.log(event.target)
}
images.addEventListener('mouseover', renderHoverBlock)