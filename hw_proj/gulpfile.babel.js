 const gulp = require('gulp'); 
const { task, parallel, watch, series, lastRun, src, dest } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const fileInclude = require('gulp-file-include');
const plumber = require('gulp-plumber');
const babel = require('gulp-babel');
const notify = require('gulp-notify');
const csso = require('gulp-csso');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const tinypng = require('gulp-tinypng');
/* const svgo = require('imagemin-svgo'); */ //Оптимизация svg
/* const svgmin = require('gulp-svgmin'); */
const imagemin = require('gulp-imagemin')


gulp.task('html', function(){
 return gulp.src('src/index.html', /* '!src/views/*.html' */)
/* .pipe(concat('src/index.html')) */
 .pipe(fileInclude({
  prefix: '@@',
  basepath: '@'
}))

.pipe(gulp.dest('dist'));
})

gulp.task('css', function(){
  return gulp.src('src/styles/style.css')
   /* .pipe(concat('style.css'))   */
    /* .pipe(sass({outputStyle: 'expanded'})) */
 
 /* .pipe(csso())  */
 .pipe(gulp.dest('dist/css'));
 })

 gulp.task('scss', function(){
   return gulp.src('src/styles/style.scss')
   .pipe(sass({
     outputStyle: 'expanded'
   }))
   .pipe(csso()) 
   .pipe(gulp.dest('dist/css'))
 })

 gulp.task('script', function(){
    return gulp.src('src/**/*.js', '!src/scripts/main.js')
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist'))
 })

gulp.task('img', function(){
  return gulp.src('src/images/**/*.*')
  /* .pipe(tinypng('1VYL7ntGv0jSl4qmNcZx7yf8rt7TnbSK')) */
  .pipe(gulp.dest('dist/pic'))
})

gulp.task('svg', function(){
 return gulp.src('src/images/**/*.*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/pic'));
  }) 



 gulp.task('default', gulp.series(
  gulp.parallel('html','scss','script'),
  gulp.parallel('img','svg')
)) 