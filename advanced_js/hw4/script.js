async function getHeroes(urls,ulList){
  let div = document.createElement('div');
  
  let requests = await urls.map(url=> fetch(url))
  Promise.all(requests)
  .then(responses=> responses.forEach(response=>response.json()
  .then(({name})=>{
  div.innerHTML += name + '* ';
   ulList.append(div)
  })
  ))
}

async function getFilms(url) {
  let div = document.querySelector('.text');
  let response = await fetch(url)
  let data = await response.json()


  const films = await data.map(async({episodeId,name,openingCrawl,characters})=> {
  let ulList = document.createElement('ul');
  ulList.insertAdjacentHTML('beforeend', `<li class='list-item'><h1>EPISODE:${episodeId}</h1><h2>NAME:${name}</h2><h4>DESCRIPTION:${openingCrawl}</h4><h3>CHARACTERS:</h3></li>`)
  console.log(characters)
  await getHeroes(characters,ulList)
  await div.append(ulList)
}) 
}

getFilms('https://ajax.test-danit.com/api/swapi/films') 