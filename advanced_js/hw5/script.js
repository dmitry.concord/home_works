class Element{
  constructor(element,class_){
    this.element = document.createElement(element)
    this.element.classList.add(class_)
  }
}

class Card extends Element {
  constructor(element,class_,attribute,name,email,title,body) {
    super(element,class_)
    this.attribute = attribute;
    this.name = name;
    this.email =email;
    this.title = title;
    this.body = body;
    this.setAtribute();
    this.setInnerHTML();
    this.render()
    this.deleteCard()
  }
  setAtribute(){
   return this.element.setAttribute('data-cards', `${this.attribute}`)
  }

  setInnerHTML(){
    return this.element.insertAdjacentHTML('beforeend',`<div class="header-name">${this.name}</div><div class="header-email"><a href='mailto:${this.email}'>${this.email}</a></div><h5>${this.title}</h5><div>${this.body}</div>`)
  }
    render(){
    document.body.append(this.element)
  }  
  add(some_element){
    this.element.append(some_element);
  }
  deleteCard(){
    this.element.addEventListener('click',(event)=>{
      const postId = event.target.parentElement.dataset.cards;
      if(event.target.tagName == 'BUTTON'){
       let request= deleteCard(postId)
       event.target.parentElement.remove()
      }
      
    })
  }
}

class Button extends Element {
  constructor(element,class_,innerText){
   super(element,class_)
   this.innerText = innerText
   this.setInnerHTML()
  }
  setInnerHTML(){
    return this.element.innerHTML = this.innerText
  }
}



async function getPosts(card_id,name,email){
  const posts = await ajaxGet('https://ajax.test-danit.com/api/json/posts');
  posts.map(({userId,title,body,id})=>{
     if(card_id == userId){
     let card = new Card('div','card',id,name,email,title,body)
     let button = new Button('button','btn','X')
     card.add(button.element)
  }}
  ) 
 }

async function deleteCard(postId) {
fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
method: "DELETE",
  }).then((response) =>{if (response.ok){return response} else return });
}

async function ajaxGet(url){
const response = await fetch(url)
const data = await response.json()
return data
}

async function getUsers(){
const users = await ajaxGet('https://ajax.test-danit.com/api/json/users');
users.map(async({id,name,email})=> {
getPosts(id,name,email)
  })

}
getUsers() 

